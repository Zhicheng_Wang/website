---
title: Homepage
description: "Description page for the website"
bookFlatSection: true
weight: 1
---

# Welcome

---

{{<img selfie_cropped_2.jpg "img_center mid shadow">}}

Hello, I am Zhicheng Wang, welcome to my page.

Currently, I'm pursuing my Master's degree in **Human-Computer Interaction** at Aalto University. 

My passion lies at the intersection of software and hardware technology, with a particular focus on **VR/AR**, **Wearable Technology**, **Haptic / Tangible Interaction Design**, and **Digital Fabrication**.

Here is my [CV](/CV.pdf).

---

You can explore additional pages for more insights into my work:
- [Digital Fabrication](/digital-fabrication), which details my experience in a six-month program where I learned about digital fabrication methods.
- [Smart Wearables](/smart-wearables), where I share details about creating a gesture-recognizing glove using textile-based sensors.
- [Embodied Interaction](/embodied-interaction), where I developed an interactive installation to experiment with physical interaction concepts.


I also enjoy exploring new media art as a hobby. You can check my [Vimeo](https://vimeo.com/zhicheng) portfolio for my previous works.