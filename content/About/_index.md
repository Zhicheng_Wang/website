---
title: About this site
description: "Description page for the author of the website"
weight: 10
---

## Why does this website exist?

Initially, this website serves as a platform for showcasing and archiving my projects related to *Digital Fabrication*:
- [AXM-E7009 Digital Fabrication I](/digital-fabrication)
- [AXM-E7010 Digital Fabrication II](/digital-fabrication)
- [AXM-E7011 Digital Fabrication Studio](/digital-fabrication)

And additionally, some other works for *Embodied Interaction*:
- [AXM-E7008 Embodied Interaction](/embodied-interaction)

After the completion of these courses, I decided to keep this website as a personal portfolio, where I can share my projects and thoughts.

---

# Timeline

- Updated on Jan 25.<br>
Homepage updated.

- Updated on June 17.<br>
Added [*Invention, Intellectual Property, and Income*](/digital-fabrication/invention-ip-and-income/) page.<br>

- Updated on June 14.<br>
Added [*Wildcard Week*](/digital-fabrication/wildcard-week/) page.<br>

- Updated on May 23.<br>
Added [*Interface and Application*](/digital-fabrication/interface-and-application/) page.<br>

- Updated on May 2.<br>
Added [*Networking and Communication*](/digital-fabrication/networking-and-communications/) page.<br>

- Updated on Apr 27.<br>
Added [*Molding and Casting*](/digital-fabrication/molding-and-casting/) page.<br>
Added [*Gesture Assignment*](/embodied-interaction/gesture-assignment/) page.<br>
Added [*Final Project*](/embodied-interaction/final-project/) page.<br>

- Updated on Apr 20.<br>
Added [*Input Device*](/digital-fabrication/input-device/) page.<br>

- Updated on Mar 30.<br>
Added [*Electronics Production*](/digital-fabrication/electronics-production/) page.<br>
Added [*Output Devices*](/digital-fabrication/output-device/) page.<br>

- Updated on Mar 16.<br>
Added [*Computer-Controlled Machining*](/digital-fabrication/computer-controlled-machining/) page.<br>

- Updated on Mar 10.<br>
Added [*Electronic Design*](/digital-fabrication/electronics-design/) page.<br>
Added [*Final Project*](/embodied-interaction/final-project/) for [*Embodied Interaction*](/embodied-interaction/)<br>
Added glow effect.

- Updated on Feb 24.<br>
Added [*3D Printing*](/digital-fabrication/3d-printing) page.<br>
Added code block highlight.

- Updated on Feb 23.<br>
Added [*Embedded Programming*](/digital-fabrication/embedded-programming) page.<br>
Added video support.

- Updated on Feb 16.<br>
Added [*Computer-aided Design*](/digital-fabrication/computer-aided-design) and [*Computer-controlled Cutting*](/digital-fabrication/computer-controlled-cutting) page.<br>
Changed base url from `zhicheng_wang.gitlab.io/website` to `zhicheng_wang.gitlab.io`.<br>
Added anchor tracking based on Matti's [Aalto New Media](https://learn.newmedia.dog/) site.<br>
Added custom css for image.<br>
Added shortcodes support for image.<br>
Adjusted theme color.


- Updated on Feb 3.<br>
Added [*Project Management*](/digital-fabrication/project-management/) page.

- Updated on Jan 26.<br>
Added [*Final Project*](/digital-fabrication/final-project/) page.

- Updated on Jan 19.<br>
Initial deployment.