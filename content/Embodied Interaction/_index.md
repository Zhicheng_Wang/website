---
title: Embodied Interaction
bookFlatSection: false
bookCollapseSection: true
weight: 3
---

# Embodied Interaction

---

## Projects

- [Shape of Time II](/embodied-interaction/final-project)

{{<img P1010507.JPG "full img_center shadow">}}

- [Gesture application](/embodied-interaction/gesture-assignment)

{{<img logo.png "small img_center ">}}


## Reference Links

- [Course website](https://learn.newmedia.dog/courses/embodied-interaction/)
- [Kinect tutorial](https://learn.newmedia.dog/courses/embodied-interaction/tools-and-technology/kinect/)
- [Final project guidelines](https://learn.newmedia.dog/courses/embodied-interaction/final-project/)
- [Showcase (final projects in the past)](https://learn.newmedia.dog/courses/embodied-interaction/showcase/)



