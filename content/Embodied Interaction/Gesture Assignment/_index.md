---
title: Gesture Assignment
weight: 10
---

# Gesture Assignment

---

## Intro
In this assignment, we are told to:
- Find a gesture that is underutilized in our everyday life
- Come up with a new interface or application for this gesture, which can take full advantages or make novel use of it
- Present this interface on a conceptual scale, no need to concern about whether it's technically achievable

## Gesture

We thought about many gesture first, such as the wiping and clapping. But then we came up with an idea of snapping, which is a commonly used gesture for different meanings, but we felt that there's more that we can dig from it. It is simple, iconic and also fun to do, these features helped us in the following sessions to find new ways to use it as well as shaping the application that was designed around that.

## Idea

For the application, we first thought about building a "snap simulator" or "snap enhancer", which is based on the fact that some people are having difficulties performing this gesture, for example struggled to snap a really loud and clear sound, so we want to make this experience more accessible for all the people. With those simulations, a snap sound effect would be played once a snap gesture is detected, thus no matter if you actually snapped a sound in the real world or not, you will hear the sound as the feedback so that everyone can have that experience. You can also change the sound effect to something else to make more interesting outcomes.

However, although this idea was quite fun, we don't think it unleash the full potential of this gesture, since it does not make any real innovations based on the gesture itself. We came up with the second idea which went on to be our final choice while we are discussing and logging in to MyCourse to see the course time arrangements. 


{{<img 0.jpg "wide img_center shadow">}}

We are all familiar with the identification interface like this. Every time we try to login to MyCourse and SISU, we will have to go through this tedious process of identification. 

These identification processes are also seen in other scenarios. Almost every online services uses their own verification method such as captcha, which can be very irritating.

{{<img id1.png "wide img_center shadow">}}

So naturally, we think about implementing the snap gesture to fix this problem. We came up with a concept application called *Snapp* that can use the snap gesture as a way of identification.

## Application

{{<img logo.png "small img_center shadow">}}

Snapp was designed to be a web browser extension in order to help people get through this identification process with a simple gesture of snap. 

Technically speaking, there isn't a way to do identification with just the gesture, at least reliably, but I think in the future if we can have high-precision 3D scanning in a more compact form factor like Apple's lidar, it is going to be achievable at some point. Compare to the traditional face identification, it have more better protection in terms of privacy. Also, I think it has more fun in this gesture, I can imaging that it would be very satisfying to perform if it was actually implemented.

There are also other applications such as the going though the long "terms of use" document and so on. We combined all that usages and our idea in a fake advertisement video. Check it out!

{{<vimeo 805034564 >}}