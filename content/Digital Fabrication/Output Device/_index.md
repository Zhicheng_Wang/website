---
title: Output Device
weight: 90
---

# Output Device

## Intro

In this page, I will document the process of using an 1.5inch OLED Module from Waveshare to the board.

---

## Basic info

{{<img oled3.jpg "wide img_center" "1.5inch OLED module from Waveshare">}}

- Controller: SSD1327
- Support interface: 4-wire SPI/I2C
- Resolution: 128 x 128
- Display size: 1.5inch
- Dimensions: 44.5mm * 37mm
- Display color: 16-bit grayscale
- Working voltage: 3.3V/5V

{{<img oled_pins.jpg "wide img_center" "Back side of the module">}}

| PIN | USAGE                         |
| --- | ----------------------------- |
| VCC | Power input                   |
| GND | Ground                        |
| DIN | Date input                    |
| SCL | Clock input                   |
| CS  | Chip select                   |
| DC  | Data/command signal selection |
| RST | Reset signal                  |

## Connections

This board can support both I2C and SPI. 

{{<img connections.jpg "wide img_center">}}

> Use 4-wire SPI: (factory setting) 
> - BS is connected to 0 and connected to GND
> - DIN is connected to the control pin MOSI
> - CLK is connected to the control pin SCK

> Using I2C: 
> - BS is connected to 1 and connected to VCC
> - DIN is connected to control pin SDA
> - CLK is connected to control pin SCL
> - CS can be left unconnected, but don't leave DC floating

Default connection settings are for the SPI connection.

Connections based on the XIAO board pins.

{{<img xinpin.jpg "img_center wide shadow" "Pins layout for Xiao board">}}

Pins used:
| xiao board PIN | oled board pin                     |
| -------------- | ---------------------------------- |
| D0             | Button / RESET                     |
| D2             | CS / Chip Select                   |
| D3             | DC / Data/command signal selection |
| D8 -- SCK      | CLK                                |
| D10 -- MOSI    | DIN                                |


## Board design

I designed the board based on the required connections. 

{{<img oled_board_sch.jpg "img_center shadow" "Schematic for OLED board">}}

{{<img oled_board_pcb.jpg "img_center shadow" "PCB design">}}

{{<img oled_board_irl.jpg "wide img_center shadow" "Board IRL">}}

There's nothing special to say about it other than avoid putting rivets to your board which brings a lot of potential connectivity troubles. At first I found some rivets have unstable contact with the pads, fixed with soldering rivets to the pads (kind of) as you can see in the picture. So far it was doing fine, but I believe there are more optimal approaches to improve that.

## Programming

### Libraries

Two libraries are tested for this module. Initially, I attempted to use the original library from Waveshare.

[Waveshare tutorial](https://www.waveshare.com/wiki/1.5inch_OLED_Module#Arduino_Tutorial)

Unfortunately it failed to compile due to font-related issues. Probably have something to do with the involve of chinese characters. 

{{<img lib1.jpg "shadow img_center">}}

Then I switched to the Adafruit library for the SSD 1327. Which worked with lots of presets and functions that are easy to use. Here's the link for that library:
- [Adafruit SSD1327](https://github.com/adafruit/Adafruit_SSD1327)

Result from the sample program. Really fancy visual effects for such a small screen.

{{<video sample_result.mp4 "shadow full img_center">}}

{{<expand "Sample Code">}}

```
#include <Adafruit_SSD1327.h>

// Used for software SPI
#define OLED_CLK SCK
#define OLED_MOSI MOSI

// Used for software or hardware SPI
#define OLED_CS D2
#define OLED_DC D3

// Used for I2C or SPI
#define OLED_RESET D0

// software SPI
Adafruit_SSD1327 display(128, 128, OLED_MOSI, OLED_CLK, OLED_DC, OLED_RESET, OLED_CS);
// hardware SPI
//Adafruit_SSD1327 display(128, 128, &SPI, OLED_DC, OLED_RESET, OLED_CS);
// I2C
//Adafruit_SSD1327 display(128, 128, &Wire, OLED_RESET, 1000000);

#define NUMFLAKES 10
#define XPOS 0
#define YPOS 1
#define DELTAY 2


#define LOGO16_GLCD_HEIGHT 16 
#define LOGO16_GLCD_WIDTH  16 
static const unsigned char PROGMEM logo16_glcd_bmp[] =
{ B00000000, B11000000,
  B00000001, B11000000,
  B00000001, B11000000,
  B00000011, B11100000,
  B11110011, B11100000,
  B11111110, B11111000,
  B01111110, B11111111,
  B00110011, B10011111,
  B00011111, B11111100,
  B00001101, B01110000,
  B00011011, B10100000,
  B00111111, B11100000,
  B00111111, B11110000,
  B01111100, B11110000,
  B01110000, B01110000,
  B00000000, B00110000 };


void setup()   {                
  Serial.begin(9600);
  //while (! Serial) delay(100);
  Serial.println("SSD1327 OLED test");
  
  if ( ! display.begin(0x3D) ) {
     Serial.println("Unable to initialize OLED");
     while (1) yield();
  }
  display.clearDisplay();
  display.display();

  // draw many lines
  testdrawline();
  display.display();
  delay(1000);
  display.clearDisplay();

  // draw rectangles
  testdrawrect();
  display.display();
  delay(1000);
  display.clearDisplay();

  // draw multiple rectangles
  testfillrect();
  display.display();
  delay(1000);
  display.clearDisplay();

  // draw mulitple circles
  testdrawcircle();
  display.display();
  delay(1000);
  display.clearDisplay();

  // draw a SSD1327_WHITE circle, 10 pixel radius
  display.fillCircle(display.width()/2, display.height()/2, 10, SSD1327_WHITE);
  display.display();
  delay(1000);
  display.clearDisplay();

  testdrawroundrect();
  delay(1000);
  display.clearDisplay();

  testfillroundrect();
  delay(1000);
  display.clearDisplay();

  testdrawtriangle();
  delay(1000);
  display.clearDisplay();
   
  testfilltriangle();
  delay(1000);
  display.clearDisplay();

  // draw the first ~12 characters in the font
  testdrawchar();
  display.display();
  delay(1000);
  display.clearDisplay();

  for (uint8_t rot=0; rot < 4; rot++) {
    display.setRotation(rot);
    display.clearDisplay();
    // text display tests
    display.setTextSize(1);
    display.setTextColor(SSD1327_WHITE);
    display.setCursor(0,0);
    display.println("Hello, world!");
    display.setTextColor(SSD1327_BLACK, SSD1327_WHITE); // 'inverted' text
    display.println(3.141592);
    display.setTextSize(2);
    display.setTextColor(SSD1327_WHITE);
    display.print("0x"); display.println(0xDEADBEEF, HEX);
    display.display();
    delay(1000);
  }

  display.setRotation(0);
  
  // miniature bitmap display
  display.clearDisplay();
  display.drawBitmap(30, 16,  logo16_glcd_bmp, 16, 16, 1);
  display.display();

  // invert the display
  display.invertDisplay(true);
  delay(1000); 
  display.invertDisplay(false);
  delay(1000); 

  // draw a bitmap icon and 'animate' movement
  testdrawbitmap(logo16_glcd_bmp, LOGO16_GLCD_HEIGHT, LOGO16_GLCD_WIDTH);
}


void loop() {
}


void testdrawbitmap(const uint8_t *bitmap, uint8_t w, uint8_t h) {
  uint8_t icons[NUMFLAKES][3];
  randomSeed(666);     // whatever seed
 
  // initialize
  for (uint8_t f=0; f< NUMFLAKES; f++) {
    icons[f][XPOS] = random(display.width());
    icons[f][YPOS] = 0;
    icons[f][DELTAY] = random(5) + 1;
    
    Serial.print("x: ");
    Serial.print(icons[f][XPOS], DEC);
    Serial.print(" y: ");
    Serial.print(icons[f][YPOS], DEC);
    Serial.print(" dy: ");
    Serial.println(icons[f][DELTAY], DEC);
  }

  while (1) {
    // draw each icon
    for (uint8_t f=0; f< NUMFLAKES; f++) {
      display.drawBitmap(icons[f][XPOS], icons[f][YPOS], bitmap, w, h, f+1);
    }
    display.display();
    delay(200);
    
    // then erase it + move it
    for (uint8_t f=0; f< NUMFLAKES; f++) {
      display.drawBitmap(icons[f][XPOS], icons[f][YPOS],  bitmap, w, h, SSD1327_BLACK);
      // move it
      icons[f][YPOS] += icons[f][DELTAY];
      // if its gone, reinit
      if (icons[f][YPOS] > display.height()) {
      	icons[f][XPOS] = random(display.width());
      	icons[f][YPOS] = 0;
      	icons[f][DELTAY] = random(5) + 1;
      }
    }
   }
}


void testdrawchar(void) {
  display.setTextSize(1);
  display.setTextWrap(false);
  display.setTextColor(SSD1327_WHITE);
  display.setCursor(0,0);

  for (uint8_t i=0; i < 168; i++) {
    if (i == '\n') continue;
    display.write(i);
    if ((i > 0) && (i % 21 == 0))
      display.println();
  }    
  display.display();
}

void testdrawcircle(void) {
  for (uint8_t i=0; i<display.height(); i+=2) {
    display.drawCircle(display.width()/2, display.height()/2, i, i % 15 + 1);
    display.display();
  }
}

void testfillrect(void) {
  uint8_t color = 1;
  for (uint8_t i=0; i<display.height()/2; i+=3) {
    // alternate colors
    display.fillRect(i, i, display.width()-i*2, display.height()-i*2,  i % 15 + 1);
    display.display();
    color++;
  }
}

void testdrawtriangle(void) {
  for (uint16_t i=0; i<min(display.width(),display.height())/2; i+=5) {
    display.drawTriangle(display.width()/2, display.height()/2-i,
                     display.width()/2-i, display.height()/2+i,
                     display.width()/2+i, display.height()/2+i,  i % 15 + 1);
    display.display();
  }
}

void testfilltriangle(void) {
  // uint8_t color = SSD1327_WHITE;
  for (int16_t i=min(display.width(),display.height())/2; i>0; i-=5) {
    display.fillTriangle(display.width()/2, display.height()/2-i,
                     display.width()/2-i, display.height()/2+i,
                     display.width()/2+i, display.height()/2+i,  i % 15 + 1);
    display.display();
  }
}

void testdrawroundrect(void) {
  for (uint8_t i=0; i<display.height()/3-2; i+=2) {
    display.drawRoundRect(i, i, display.width()-2*i, display.height()-2*i, display.height()/4,  i % 15 + 1);
    display.display();
  }
}

void testfillroundrect(void) {
  // uint8_t color = SSD1327_WHITE;
  for (uint8_t i=0; i<display.height()/3-2; i+=2) {
    display.fillRoundRect(i, i, display.width()-2*i, display.height()-2*i, display.height()/4,  i % 15 + 1);
    display.display();
  }
}
   
void testdrawrect(void) {
  for (uint8_t i=0; i<display.height()/2; i+=2) {
    display.drawRect(i, i, display.width()-2*i, display.height()-2*i, i % 15 + 1);
    display.display();
  }
}

void testdrawline() {  
  for (uint8_t i=0; i<display.width(); i+=4) {
    display.drawLine(0, 0, i, display.height()-1, SSD1327_WHITE);
    display.display();
  }
  for (uint8_t i=0; i<display.height(); i+=4) {
    display.drawLine(0, 0, display.width()-1, i, SSD1327_WHITE);
    display.display();
  }
  delay(250);
  
  display.clearDisplay();
  for (uint8_t i=0; i<display.width(); i+=4) {
    display.drawLine(0, display.height()-1, i, 0, SSD1327_WHITE);
    display.display();
  }
  for (int8_t i=display.height()-1; i>=0; i-=4) {
    display.drawLine(0, display.height()-1, display.width()-1, i, SSD1327_WHITE);
    display.display();
  }
  delay(250);
  
  display.clearDisplay();
  for (int8_t i=display.width()-1; i>=0; i-=4) {
    display.drawLine(display.width()-1, display.height()-1, i, 0, SSD1327_WHITE);
    display.display();
  }
  for (int8_t i=display.height()-1; i>=0; i-=4) {
    display.drawLine(display.width()-1, display.height()-1, 0, i, SSD1327_WHITE);
    display.display();
  }
  delay(250);

  display.clearDisplay();
  for (uint8_t i=0; i<display.height(); i+=4) {
    display.drawLine(display.width()-1, 0, 0, i, SSD1327_WHITE);
    display.display();
  }
  for (uint8_t i=0; i<display.width(); i+=4) {
    display.drawLine(display.width()-1, 0, i, display.height()-1, SSD1327_WHITE); 
    display.display();
  }
  delay(250);
}

```

{{</expand>}}

### Gyro visualization

After trying out the functions for the display, I decided to make a simple visualization for the Gyroscope that I used in the [Input device](/digital-fabrication/input-device) week.

The goal is to visualize the three axes (pitch, roll, yaw) data. I used a simple cursor which contains a circle and a triangle to indicate that. The pitch and roll dimensions are depicted as the X and Y coordinates of the cursor on the screen, while the dimension of yaw was presented by the rotation of the cursor.

To draw the cursor, firstly I calculated the center coordinate by mapping the pitch and roll angle to the screen size, and then draw the circle around that:

```
// Calculate coordinate
x = map(long(ax),90,-90,0,display.width());
y = map(long(ay),-90,90,0,display.height());

// Draw circle
display.drawCircle(x, y, radius, SSD1327_WHITE);
```

After that, I calculate the three points for drawing the triangle:

```
// Draw triangle
rad1 = az / 180 * M_PI;
rad2 = (az+120) / 180 * M_PI;
rad3 = (az+240) / 180 * M_PI;
display.drawTriangle(int(x + sin(rad1)*t_size), int(y + cos(rad1)*t_size),  
                int(x + sin(rad2)*t_size), int(y + cos(rad2)*t_size),  
                int(x + sin(rad3)*t_size), int(y + cos(rad3)*t_size), SSD1327_WHITE);
display.display();

```

Here is the final result I have achieved. 

{{<video final_result.mp4 "img_center full shadow">}}

The visualization is straightforward and provides accurate representation of the data. I do wish that there are more functions for draw so that I can display 3D or fake 3D effects on the screen. I will look into that in the [Interface and Application](/digital-fabrication/interface-and-application) week.

{{<expand "Gyro visualization Code">}}
```
#include <Adafruit_SSD1327.h>
#include "Wire.h"
#include <MPU6050_light.h>
#include "math.h"


// Settings for MPU6050 Accelero/Gyro
MPU6050 mpu(Wire);
unsigned long timer = 0;

// Angle data from gyro
float ax = 0;
float ay = 0;
float az = 0;

// Coordinate and parameters for drawing
int x = 0;
int y = 0;
const int radius = 10;
const int t_size = 10;
float rad1 = 0;
float rad2 = 0;
float rad3 = 0;


// Settings for OLED
#define OLED_CLK SCK
#define OLED_MOSI MOSI
#define OLED_CS D2
#define OLED_DC D3
#define OLED_RESET D0
Adafruit_SSD1327 display(128, 128, OLED_MOSI, OLED_CLK, OLED_DC, OLED_RESET, OLED_CS);


void setup() {

  Serial.begin(9600);
  Wire.begin();
  
  //OLED start
  Serial.println("SSD1327 OLED test");
  
  if ( ! display.begin(0x3D) ) {
     Serial.println("Unable to initialize OLED");
     while (1) yield();
  }
  display.clearDisplay();
  display.display();

  display.setTextSize(1);
  display.setTextColor(SSD1327_WHITE);
  

  //MPU start
  byte status = mpu.begin();
  Serial.print(F("MPU6050 status: "));
  display.println("MPU6050 status: ");
  Serial.println(status);
  display.println(status);
  while(status!=0){ } // stop everything if could not connect to MPU6050
  Serial.println(F("Calculating offsets, do not move MPU6050"));
  display.println("Calculating offsets, do not move MPU6050");
  display.display();
  delay(1000);
  // mpu.upsideDownMounting = true; // uncomment this line if the MPU6050 is mounted upside-down
  mpu.calcOffsets(); // gyro and accelero
  Serial.println("Done!\n");
  display.clearDisplay();
  display.display();


}

void loop() {
  mpu.update();
  ax = mpu.getAngleX();
  ay = mpu.getAngleY();
  az = mpu.getAngleZ();

  Serial.print(ax);Serial.print(",");
  Serial.print(ay);Serial.print(",");
  Serial.print(az);Serial.println();

  display.clearDisplay();
  // Print angle data
  display.setCursor(0,0);
  display.print(ax);display.println(",");
  display.print(ay);display.println(",");
  display.print(az);display.println();

  // Calculate coordinate
  x = map(long(ax),90,-90,0,display.width());
  y = map(long(ay),-90,90,0,display.height());

  // Draw circle
  display.drawCircle(x, y, radius, SSD1327_WHITE);

  // Draw triangle
  rad1 = az / 180 * M_PI;
  rad2 = (az+120) / 180 * M_PI;
  rad3 = (az+240) / 180 * M_PI;
  display.drawTriangle(int(x + sin(rad1)*t_size), int(y + cos(rad1)*t_size),  
                  int(x + sin(rad2)*t_size), int(y + cos(rad2)*t_size),  
                  int(x + sin(rad3)*t_size), int(y + cos(rad3)*t_size), SSD1327_WHITE);
  display.display();
}
```
{{</expand>}}