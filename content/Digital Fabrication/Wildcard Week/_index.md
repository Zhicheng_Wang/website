---
title: Wildcard Week
weight: 150
---

# Wildcard Week - Waterjet Cutting

## Intro

In this page, I will document the process of making an aluminum keyboard case with waterjet cutting.

---

## Keyboard Design

I have been using an ergo mechanical keyboard called Spring for a while, it was made with acrylic which is a material that is not very durable, thus a new case with aluminum was planned.

{{<img spring_original.jpg "full shadow img_center" "Mom I want a Spring keyboard at home">}}

{{<img spring_acrylic_stack.jpg "wide shadow img_center" "The one we actually have:">}}

Since the original design was a stack design, it was not very hard to convert from the acrylic design to the aluminum design.

{{<img design.jpg "wide shadow img_center" "The design">}}

The only thing that I had to do was to change some distances between the layers to make sure that the screws would fit. Unfortunately, there are still some problems that I failed to notice, which will be discussed later.

## File Preparation

As we do not had the introduction session before the wildcard week, there are some problems that I encountered during the process of preparing the file for the waterjet cutting machine. The files I prepared was `.dwg` exported from Adobe Illustrator, in which I adjusted the alignment of the parts. However, multiple issue including the wrong file format and scaling problem occurred. And I spent half an hour to fix the file before the cutting started.

To prevent this unnecessary time waste from happening, several things should be noted:

{{<hint info>}}

- The file should be in `.dxf` format.
- The file should be in millimeter unit.
- There should be no overlapping lines.
- Shapes should be closed unless intended.
- Kerf should not be taken into account, the toolpath generator will take care of it.
- Add tabs to the small parts to prevent them from falling off the machine bed, or change the orientation of the parts so that they are not parallel to the bed.

{{</hint>}}

After the file was fixed it was sent to *Kari*, the workshop master. He uses Rhino to check the scale and arrange the parts on the bed. He made some minor adjustments to the file, changing the direction of the parts for a tighter layout that saves material. After confirming that the file is good, we can proceed to the next step.

## Machine Information

The machine was prepared by Kari as we do not have the permission to operate. The machine we have here in Aalto workshop is the **Waterjet cutter Omax 55100**. You can find more information about that here:

- [Workshops - Aalto School of Arts, Design and Architecture - Waterjet Cutting](https://www.aalto.fi/en/workshops-aalto-school-of-arts-design-and-architecture/waterjet-cutting)
- [Product page from manufacture: OMAX 55100 PRECISION JETMACHINING CENTER](https://www.omax.com/en/us/omax-waterjet/55100)

Also Darren's blog has a very detailed documentation of the features, usage, and technological introduction of the machine:
- [Darren Bratten - Week 18: Wildcard Week](https://darrenbratten.gitlab.io/digital-fabrication_2022/wk18_wildcard-week.html)

The basic information of the machine is listed below:
| Parameter                | Value             |
| ------------------------ | ----------------- |
| X-Y CUTTING TRAVEL       | 2540 mm x 1397 mm |
| Z-AXIS TRAVEL            | 203 mm            |
| Linear position accuracy | ±0.025 mm         |

All in all, the machine has a huge cutting area, and is very precise considering the size of the machine. However, due to the way that waterjet cutting works, the deeper the cuts, the more spread the waterjet will be, thus the quality of the cut will be worse. Therefore, the best use case for this machine is to cut thin but large material sheets.

## Cutting Process

Once the design drawing is verified or confirmed to be without errors, it will be sent to the machine operator's station for further processing.

{{<img waterjet_operator_station.jpg "wide shadow img_center" "The operator station">}}

There, the design will be further evaluated and optimized by the control software. Numbers of overlapping shapes will be listed, and duplicated shapes will be removed. In this stage we can still do some final adjustments for the alignments of the parts.

{{<img waterjet_control_software.jpg "wide shadow img_center" "The control software">}}

After the design is confirmed and the dimensions of the cut was settled, we started to prepare the material on the machine bed. The material I used is a 80*100cm big, 3mm thick aluminum sheet. 

{{<img waterjet_material.jpg "wide shadow img_center" "The material (after the cut)">}}

The material was fixed on the bed through clamps.

{{<img waterjet_clamps.jpg "wide shadow img_center" "Fixing material">}}

Toolpath will then be generated. Notice that kerf is taken into account automatically here.

{{<img waterjet_toolpath.jpg "wide shadow img_center" "Toolpath">}}

The zeroing process is pretty similar to MDX-40. XY origin zeroing was performed with naked eye, aligning the nozzle to mark that was left before by the calipers. Z axis zeroing was performed with a special metal tool that goes between the nozzle and the bed to check if the distance was appropriate.

{{<img waterjet_zeroing.jpg "wide shadow img_center" "Zeroing">}}

After zeroing is done, the cover for the nozzle will be lowered, and we start cutting.

{{<img waterjet_prepared.jpg "wide shadow img_center" "Ready to cut">}}

{{<hint warning>}}
The cutting process is fairly loud, it is recommended to wear ear protection during the cutting process.
{{</hint>}}

{{<video waterjet_cutting_x264.mp4 "full shadow" "And we are cutting!">}}

{{<img waterjet_result_stage1.jpg "wide shadow img_center" "The end of cutting session one">}}

Due to flaws in designing and alignment, small parts are placed paralleled with the gaps on the machine bed, which caused them to fall off the bed.

{{<video waterjet_falling_x264.mp4 "full shadow" "One successful cut and one failed cut">}}

These losses in resulted in an insufficient total number of parts, thus we conducted the second cutting session covering the remaining parts. This time the pars are aligned with a 45 degree angle to the bed gaps.

{{<img waterjet_new_toolpath.jpg "wide shadow img_center" "Regenerating the toolpath">}}

{{<img waterjet_result_stage2.jpg "wide shadow img_center" "Cutting session two">}}

{{<img waterjet_result.jpg "wide shadow img_center" "The final result">}}


There are some take home messages that I learned from this process:

{{<hint info>}}
- Always check the file format and unit before sending the file to the machine. A separate CAD software (other than the main one that the design is made) is recommended for checking the file.
- Always check the design, especially if the lines are properly closed and there are no overlapping.
- The machine bed is extremely sharp since it was worn out by the waterjet, thus if you want your cutting surface to me nice and smooth, a sacrificial layer would be recommended to be placed underneath the material.
- Front surface and bottom have different flaws. For front surface the cuts are smoother but there's a small spray where the waterjet first goes through the material. For the bottom surface the cuts are rougher but there's no visible artifacts.
{{</hint>}}

## Assembly

After heading back to Fablab, I sanded roughly and then washed the parts.

{{<img washed.jpg "full shadow img_center" "Lookin' good after cleaning">}}

I discoverd that the cutting still have error, however I think probably the main issue is the flexure of the material since it's relatively thin.

{{<img error.jpg "full shadow img_center" "The error">}}

When assembling I discovered that the stand have some design flaws, since the original stand uses 5mm acrylic and here it was 3mm aluminum. I decided to improvise a little by and change the layer order and adding some 4mm plywood pieces.

{{<img stands_comparison.jpg "full shadow img_center" "Comparison between old and new stand">}}

{{<img stands_assembled.jpg "full shadow img_center" "Stands assembled">}}

{{<img stand_for_pcb.jpg "full shadow img_center" "Installed support for the pcb">}}

{{<img pcb_and_plate.jpg "full shadow img_center" "Installed pcb and plate">}}

{{<img fully_assembled.jpg "full shadow img_center" "All done">}}

Overall, I am pleased with the cutting process as it turned out to be more successful than I expected. The only thing that I would like to improve is the design of the stand, but since it was hidden underneath so normally you won't notice. Nevertheless, I am genuinely satisfied with the outcome. Probably the only aluminum stacked Spring keyboard in the world!