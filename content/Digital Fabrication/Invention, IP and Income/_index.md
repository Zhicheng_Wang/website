---
title: Invention, IP and Income
weight: 180
---

# Invention, IP and Income

## Intro

In this page, I will discuss the future plan of my final project, and also document the process of learning about the intellectual property and different licensing methods, and finally choosing one of the licenses for my project.

---

## Future Plan

My final project is a machine setup that generates video feedback loop. You can have more information about the project here: [Final project page](/digital-fabrication/final-project). 

### Technical Improvement

Technically speaking, there are still lots of room for the project to be improved. The current state of the project could be seen as a fesability test of the whole idea rather than a finished product. The following are some of the things that I would like to improve in the future:

- Camera improvement

{{<img esp-cam.JPG "img_center wide shadow">}}
The camera used in the current state is a ESP32-CAM module which features a OV2640 sensor. This setup is fine for capturing basic image, but fails to capture details and textures from the screen due to its low resolution, dynamic range and color depth. Also, the video transmitting speed is capped by the limits of computational power of the ESP32 chip. A higher-end camera module or a dedicated DSLR camera would be a better choice, providing better image quality and higher video transmitting speed for more complex video feedback effects.

- Camera control

Camera controls are done by servo motors at this moment, for the whole camera setup is compact and light-weight at this moment. However, if a bigger camera setup or module will be used in the future, a more powerful motor would be needed, like a stepper motor. Also, the current setup is not very stable, and the camera is not very well fixed. A more stable and reliable camera control system would be needed.

- Transmission

{{<img rtsp.jpg"img_center wide shadow" "The way of how RTSP protocol works">}}
Currently, the video is transmitted from the camera to the computer via WiFi, and the receive client can get the video stream using http requests. This method is fine for just viewing the video in browser, but it the video media stream was needed to be put into TouchDesigner or other post processing software, a more reliable and stable transmission method based on a streaming protocol would be needed. RTSP would be a good choice, but having it running on the ESP32 chip would be a challenge.

### Overall Plan

As an installation art, I wouldn't consider it to be commercialized in the future. However, I would like to make it more accessible to the public, and also make it more stable and reliable. I wish that this project would serve as a good example or reference when people are trying to make their own video feedback loop setup.

## Intellectual Property

Throughout the course and the process of making the final project, I gathered lot of information from others, including random guys from internet, and of course, previous students of Digital Fabrication and Fab Academy. However during this process, I hardly paid attention to the intellectual property when I gather them, I just took the information and used it in my project without thinking about it. Luckily, as most of them are open sourced and I didn't use them for commercial purpose, everything should be fine as I mentioned the references correctly.

Nonetheless, I still think it is important and relevant to learn about the intellectual property and start to apply the licenses to the contents that I created, so that both the creator and the user of the information could be protected.

I learned about three different licenses for different content and use cases, which are the MIT license, the Creative Commons license, and the GNU General Public License. Quoted contents are generated from ChatGPT.

### MIT License

- [The MIT License](https://opensource.org/licenses/MIT) 

{{<hint>}}

The MIT License is a widely used open-source software license that grants permission to use, modify, and distribute software. It was originally created by the Massachusetts Institute of Technology (MIT) and has become one of the most permissive and popular licenses in the open-source community.

- Permissions: 

The MIT License allows anyone to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the software, both in source code and binary form, without any restrictions.

- Liability and Warranty: 

The license includes a disclaimer of liability, stating that the software is provided "as is" without any warranty or guarantee of its performance or suitability for any particular purpose. The license holder assumes all risks associated with the use of the software.

- Attribution: 

The license requires that the original copyright notice and the license terms are included in all copies or substantial portions of the software.

- Compatibility: 

The MIT License is considered a permissive license, meaning it can be combined or incorporated into projects under different licenses. It is often used alongside other open-source licenses, making it easy to integrate MIT-licensed software into larger projects.

- Commercial Use: 

The MIT License explicitly permits commercial use, allowing individuals or organizations to use the software for commercial purposes without any additional requirements.

{{</hint>}}

Overall, I think the MIT license provides a pretty straightforward and permissive framework for sharing and using open-source software. It is funny to read about the liability and warranty part considering how badly we wrote our own code, which kind of making this license more suitable than it should. Anyways, I will add this license to the code files of my project.

### Creative Commons License

- [Creative Commons License](https://creativecommons.org/)

{{<hint>}}

The Creative Commons (CC) licenses are a set of copyright licenses that provide a standardized and flexible way to grant permissions for the use, sharing, and modification of creative works. These licenses were developed by Creative Commons, a non-profit organization dedicated to enabling the legal sharing and distribution of creative content.

- Different License Options: 

Creative Commons offers several license options, each with varying levels of permissions and restrictions. The main licenses include Attribution (CC BY), Attribution-ShareAlike (CC BY-SA), Attribution-NoDerivs (CC BY-ND), Attribution-NonCommercial (CC BY-NC), Attribution-NonCommercial-ShareAlike (CC BY-NC-SA), and Attribution-NonCommercial-NoDerivs (CC BY-NC-ND). These licenses provide different combinations of requirements such as attribution, share-alike, no derivatives, and non-commercial use.

- Permissions and Restrictions: 

The CC licenses are designed to be more flexible than traditional copyright. They enable content creators to choose the permissions they want to grant to others while retaining some control over their work. The licenses allow for the use, sharing, adaptation, and even commercial use of creative works, depending on the specific license chosen.

- Attribution Requirement: 

Most of the CC licenses require attribution, meaning that users of the licensed work must give appropriate credit to the original creator, including the title, author's name, and a link to the original work.

- Compatibility: 

The CC licenses are widely used and compatible with other open content and open-source licenses. This allows for greater collaboration and sharing between different projects and communities.

- Global Reach: 

The CC licenses are internationally recognized and used in countries around the world. They provide a legal framework that helps content creators and users navigate copyright laws and facilitates the global sharing of creative works.

{{</hint>}}

Overall, the Creative Commons licenses provide a standardized and flexible framework for sharing and using creative works. I will use this license for the documentation of my project.

You can choose the license that best suits your needs and requirements here: [Choose a License](https://creativecommons.org/choose/). I chose the *Attribution-NonCommercial-ShareAlike 4.0 International* license for my documentation since I want it to be remained open-sourced and free to use for everyone.

### GNU General Public License

- [GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.html)

{{<hint>}}
GNU licenses, also known as GNU General Public Licenses (GPL), are a set of widely used free software licenses that were created by the Free Software Foundation (FSF). These licenses aim to protect and preserve the freedom to use, study, modify, and distribute software.

- Copyleft Principle: 

The GNU licenses are based on the copyleft principle, which ensures that the software and any derivative works remain free and open. Copyleft licenses require that the source code of the software, along with any modifications or additions, must be made available to users under the same license terms.

- GPL Versions: 

The GNU licenses come in different versions, the most commonly used being GPL version 2 (GPLv2) and GPL version 3 (GPLv3). Each version has its own specific terms and conditions, but they share the core principles of copyleft and freedom.

- Distribution and Modification: 

The GNU licenses grant users the freedom to distribute, modify, and even sell the software, as long as they comply with the license conditions. These licenses encourage collaboration and sharing within the free software community.

- Strong Copyleft: 

The GNU General Public License (GPL) is considered a strong copyleft license. If you use GPL-licensed software in your own project and distribute it to others, your project must also be licensed under the GPL, ensuring that the freedom and openness of the original software are preserved.

- Compatibility: 

The GNU licenses are compatible with other free software licenses like the Apache License and MIT License, allowing for code sharing and collaboration between projects under different licenses. However, it's important to carefully consider the license compatibility and requirements when combining software under different licenses.

- Ethical Considerations: 

The GNU licenses are rooted in the philosophy of software freedom and promote ethical considerations such as user freedom, transparency, and community collaboration. They strive to protect users from proprietary restrictions and enable the development of software that benefits the entire society.

{{</hint>}}

Overall, I think the GNU licenses have played a significant role in the free software movement and have helped foster the development and widespread use of open-source software, I think a lot of linux softwares uses them which is probably the reason why I have a recollection of having heard of it before. I will use this license for the design and the software (if there will be) of my project.

## Adding License to the Website

I added the license to the website by injecting the following code to the menu of the website:

```
<hr>
<div class="nav-footer">
    <p>
        This work is licensed under a 
        <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
            Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License
        </a>.
    </p>
    <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
        <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png"/>
    </a>
</div>
```

Now it can be observed on the left side of the page, positioned in the footer section beneath the menu.

## Final Project Video and Slides

You can access the videos and slides for the final project here:

{{<img "https://zhicheng_wang.gitlab.io/presentation.png" "full shadow img_center" "Kaleidoscope, a Video Feedback Generator">}}

{{<video "https://zhicheng_wang.gitlab.io/presentation.mp4" "full">}}

You can also view all the slides and videos for the final project of digital fabrication 2023 [here](https://aaltofablab.gitlab.io/digital-fabrication-2023/).