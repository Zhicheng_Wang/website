---
title: Project Management
description: "Description page for the process of deploying and organizing content for Digital Fabrication"
weight: 10
---

# Project Management

## Intro
In this page two main contents will be presented:
- Process of deploying this website to GitLab
- Tools and methods that I used to process, organize and manage media for this website

---

## Deploying the website
### Preparation for GitLab

 - Setup the account
 - Verify email
 - Verify bank account


![gitlab account](gitlab_account.jpg)

### SSH Keys
I use this command to generate public and private keys
```
$ ssh-keygen -t ed25519 -C "my_email@example.com"
```
Then added to GitLab to access and write data in repositories.

![sshkey](sshkey.png)


### Preparation for building website locally
- Install Go
- Install Hugo (extended edition)
- Install Git (already installed long time ago)
- Follow the [**Hugo quick start tutorial**](https://gohugo.io/getting-started/quick-start/)

### Build site locally
- Add files according to [**Branch Bundle**](https://gohugo.io/content-management/page-bundles/)

```
/content
│  _index.md
│
├─About
│      selfie.jpg
│      _index.md
│
└─Digital Fabrication
    │  _index.md
    │
    ├─Final Project
    │      libra62.jpg
    │      _index.md
    │
    └─Project Management
            gitlab_account.jpg
            sshkey.png
            _index.md
```

- Use `hugo server` command to run site locally


```
(base) D:\website\blog_gitlab>hugo server
Start building sites …
hugo v0.109.0-47b12b83e636224e5e601813ff3e6790c191e371+extended windows/amd64 BuildDate=2022-12-23T10:38:11Z VendorInfo=gohugoio
                   | EN
-------------------+-----
  Pages            | 15
  Paginator pages  |  0
  Non-page files   |  2
  Static files     | 79
  Processed images |  0
  Aliases          |  2
  Sitemaps         |  1
  Cleaned          |  0

Built in 151 ms
Watching for changes in D:\website\blog_gitlab\{archetypes,assets,content,data,layouts,static,themes}
Watching for config changes in D:\website\blog_gitlab\config.toml
Environment: "development"
Serving pages from memory
Running in Fast Render Mode. For full rebuilds on change: hugo server --disableFastRender
Web Server is available at http://localhost:1313/website/ (bind address 127.0.0.1)
Press Ctrl+C to stop
```

### Make customizations
- Add `_custos.scss` and `_variable.scss` to `/assets` to make customizations to the color palate of ToC and menu navigation of the `hugo-book-theme`

```
@mixin theme-dark {
  --gray-100: rgba(255, 255, 255, 0.1);
  --gray-200: rgba(255, 255, 255, 0.2);
  --gray-500: rgba(255, 255, 255, 0.5);

  --color-link: #b5c3d4;
  --color-visited-link: #949494;

  --body-background: #121212;
  --body-font-color: #ffffff;

  --icon-filter: brightness(0) invert(1);

  --hint-color-info: #90ddca;
  --hint-color-warning: #fd6;
  --hint-color-danger: #f66;
}
```

### Deploy site to GitLab Pages
- Add a `.gitlab-ci.yml` file as the [documentation](https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/) instructed

```
image: registry.gitlab.com/pages/hugo/hugo_extended:latest

variables:
  GIT_SUBMODULE_STRATEGY: recursive

pages:
  script:
  - hugo
  artifacts:
    paths:
    - public
  rules:
  - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```

- Use `git` commands to push the local repositories to GitLab, CI/CD pipline will deploy the website automatically

```
git add .
git commit -m "inital push"
git push origin main
```

![gitlab pipline](gitlab_pipline.jpg)

***

## Tools for managing media
### Image resizer

![imgae resizer](https://learn.microsoft.com/en-us/windows/images/powertoys-resize-images.gif)

[*Image Resizer*](https://learn.microsoft.com/en-us/windows/powertoys/image-resizer) is a Windows shell extension for bulk image-resizing, and now it's embedded into PowerToys. To use it, after installing [*PowerToys*](https://learn.microsoft.com/en-us/windows/powertoys/), right-click on one or more selected image files in File Explorer, and then select Resize pictures from the menu. This is a super useful tool for compressing large image that were unnecessary high-res for the website.



### Maruko Toolbox

{{< img xiaowan.jpg "img_center shadow wide">}}

[*Maruko Toolbox*](https://maruko.appinn.me/index.html) is a software for processing multimedia files, both audio and video. It is a GUI for command-line programs such as `x264` and `ffmpeg`. Compare to the command line, the Maruko Toolbox is more user-friendly and simple, and is perfectly suitable for light tasks such as compressing videos for the website.

It's main function includes:
- High quality H264+AAC video compression
- Embed ASS/SRT subtitles into video
- AAC/WAV/FLAC/ALAC audio conversion
- Lossless extraction and packaging of MP4/MKV/FLV
