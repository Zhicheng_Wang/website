---
title: Final Project
description: "Description page for the final project of Digital Fabrication"
bookFlatSection: false
bookCollapseSection: true
weight: 1
---

# Final Project - Kaleidoscope



{{<img "1 presentation.png" "img_center shadow">}}

***Kaleidoscope*** is a modern reinterpretation of the classic analog video feedback loop, but designed in and for the digital era. 

{{<video "https://zhicheng_wang.gitlab.io/presentation.mp4" "full">}}

---


The installation comprises a 3-axis camera stand powered by three separate servo motors and equipped with laser-cut and 3D printed components. It can be controlled with a joystick or remotely via Wi-Fi network.

{{<img "2 camera stand.png" "img_center shadow">}}

Additionally, it features a versatile camera module based on the esp32-CAM board, capable of capturing images and simultaneously streaming wirelessly through a hosted server. 

{{<img "3 cam module boards.png" "img_center shadow wide">}}

The video signal output is displayed on a CRT TV and then captured by the camera again, thus creating this feedback loop. The dynamic visual effects are achieved through subtle adjustments in the position, distance, and angle of the camera, introduced by the camera stand.

{{<img "4 effects.png" "img_center shadow">}}

> Watch in awe as it transforms the mundane into the mesmerizing, while drowning the infinite loop of ever changing perception.

---

For more information including the ideation process and weekly progress tracking, check the following pages:

- [Weekly updates](/digital-fabrication/final-project/weekly-progress/) - weekly diaries of the project development process.
- [References](/digital-fabrication/final-project/references/) - compilation of reference videos, artworks, and articles that have inspired and informed the project's development.
- [Abandoned Ideas](/digital-fabrication/final-project/abandoned-ideas/) - other ideas that I have envisionlized for the final project.

---
