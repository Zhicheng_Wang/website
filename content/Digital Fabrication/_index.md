---
title: Digital Fabrication
description: "Description page for the Digital Fabrication course"
bookFlatSection: false
bookCollapseSection: true
weight: 1
---

# **Digital Fabrication**
***
{{<img DF2023banner.jpg "img_center full shadow">}}
## Introduction
The *Digital Fabrication* courses at Aalto University are based on [*The Fab Academy's*](https://fabacademy.org/) principles, a program founded by Neil Gershenfeld and adopted globally. It's a hands-on, fast-paced learning experience where students develop rapid-prototyping skills through weekly projects, culminating in a personal technical skills portfolio.

Spanning 5-6 months, these courses cover various digital fabrication techniques, including CAD design, 3D printing, laser cutting, CNC machining, etc. The curriculum is designed to give students a thorough understanding of these technologies, preparing for the evolving field of digital fabrication.

I was drawn to these courses due to my interest in electronic design and production. Recognizing the limitations of traditional methods used in prototyping, I'm excited to delve into innovative fabrication methods, combining them and anticipating new discoveries in this advancing field.

{{<img DF2023.jpg "img_center wide shadow" "A photo featuring the students and instructor of the DF2023 Community :)">}}

## Documentations
- [Week 1 - Project management](/digital-fabrication/project-management)
- [Week 2 - Computer-Aided Design](/digital-fabrication/computer-aided-design)
- [Week 3 - Computer-Controlled Cutting](/digital-fabrication/computer-controlled-cutting)
- [Week 4 - Embedded Programming](/digital-fabrication/embedded-programming)
- [Week 5 - 3D Printing](/digital-fabrication/3d-printing)
- [Week 6 - Electronic Design](/digital-fabrication/electronics-design)
- [Week 7 - Computer-Controlled Machining](/digital-fabrication/computer-controlled-machining)
- [Week 8 - Electronic Production](/digital-fabrication/electronics-production/)
- [Week 9 - Output Device](/digital-fabrication/output-device/)
- [Week 10 - Machine Building](/digital-fabrication/machine-building/)
- [Week 11 - Input Device](/digital-fabrication/input-device/)
- [Week 12 - Molding and Casting](/digital-fabrication/molding-and-casting/)
- [Week 13 - Networking and Communications](/digital-fabrication/networking-and-communications/)
- [Week 14 - Interface and Application](/digital-fabrication/interface-and-application)
- [Week 15 - Wildcard Week](/digital-fabrication/wildcard-week/)
- [Week 16 - Applications and implications](/digital-fabrication/applications-and-implications)
- [Week 17 - Invention, Intellectual Property, and Income](/digital-fabrication/invention-ip-and-income)
- [Week 18 and more - Final Project development](/digital-fabrication/final-project/weekly-progress)


## Archive
- [Digital Fabrication 2023 Archive](https://gitlab.com/aaltofablab/digital-fabrication-2023)
- [Digital Fabrication 2022 Archive](https://gitlab.com/aaltofablab/digital-fabrication-2022)
- [MIT - Lining Yao](http://fab.cba.mit.edu/classes/863.11/people/lining.yao/index.html#Projects)

## Links

- [Global course zoom link](http://fabacademy.org/2022/video.html)
- [Fab Academy 2023](https://fabacademy.org/2023/schedule.html)
- [Fab Inventory](https://inventory.fabcloud.io/)