---
title: Electronics Production
weight: 80
---

# Electronics Production


## Intro

In this page, I will document the process of fabricating a pcb board using the SRM-20 milling machine.

---

## PCB design

The pcb which I was going to fabricating is from the electronics design week. You can get more information for the original design here.

However, as I was preparing for the fab process, I found that several issue occurs and I have to make major changes on the pcb design before the fab process. Several components are out of stock in fablab, such as pin sockets and SSD 1306 oled display.

{{<img schematic.jpg "img_center" "Schematic of the board">}}


{{<img pcb_4.jpg "wide img_center" "First version">}}

{{<img pcb_final.jpg "wide img_center" "Final version">}}

## Export from KiCAD

The first step of fabricating a pcb is to export the pcb design into `gerber` files. 

For our fabricating processes, the gerber files that we concern about is the files for copper layer and the drilling holes. Usually the edge cut layer is plotted within each file, so no need for a separate one.

The settings are as follows:

{{<img gerber_settings.jpg "wide img_center">}}

{{<img drill_settings.jpg "half img_center">}}



## CopperCAM

In CopperCAM there are several things to do:

- Import gerber files (copper layer and drills)
- Align drill holes to copper layer
- set dimension and origin
- adjust tool settings
- generate tool path
- (adjust tool path)
- save tool path

### Import

Import `yourProjectName_F_Cu.gbr` first, which will contain the copper layer as well as the edge cut layer.

Import `yourProjectName-PTH-drl.gbr` second, which contains the holes.

Should get something like this:

### Alignment

> Select copper layer (layer 1), set as reference pad.
> 
> Select drill layer (layer 5), adjust to reference pad.

Should get something like this:

{{<img final-cut.jpg "wide img_center">}}

For me there's no rotation issue. But if there is:

> Select drill layer (layer 5), File --> Rotate 90

### Set dimension and origin

> File --> Dimension / Origin

{{<img COPCAM_dimensions.jpg "half img_center">}}

> Margin --> 1, Origin --> (0,0)
> 
> Thickness not very important, adjust later in tool settings for cutting and drilling.

### Adjust tool settings

{{<img COPCAM_tools.jpg "wide img_center">}}

As my pcb design have a USB C port which have very thin pads, I have to use the smaller engraving tool which is the 0.1-0.3mm engraving tool. This tool have a slower feed rate of 2mm/s. For the 0.2-0.5mm tool, a feed rate of 10mm/s is fine. For smaller tools, a more shallow engraving depth is recommended, for example I used 0.08mm for the 0.1-0.3mm tool, and probably 0.1mm is fine for the 0.2-0.5mm tool.

| Tool      | Feed rate | Engraving depth |
|-----------|-----------|-----------------|
| 0.1-0.3mm | 2mm/s     | 0.08mm          |
| 0.2-0.5mm | 10mm/s    | 0.1mm           |

Drilling and cutting can share the same tool. 

The cutting depth should be the identical with the `board_thickness` (with the tape counted in). For my case it's 1.76mm. 

For drilling depth we usually use `cutting_depth + 1mm` to make sure the drills are fully through the pcb, so that 1.86mm in my case.

### Contours Setting

Two options for generate contours
- Number of successive contours: How many times the bit will engrave outside the tracks and pads. Usually, 3~4 is good.
- Extra contours around pads: How many times the bit will engrave around the pads after successive contours.

It's important to set enough contours for pads so that everything is separated correctly.



### Export tool path

Save tool path for different tools.

{{<img COPCAM_savepath.jpg "wide img_center">}}



## Using the SRM-20

1. Clean the machine bed surface
2. Tape the back side of the pcb board, **measure the thickness**
3. Install the tool
{{<img "tool.jpg" "wide img_center">}}

4. Fix the board on the bed
5. Set the Z origin

{{<img "vpanel.JPG" "wide img_center">}}
- Set at the center of the cut
- Push the bit down a little to the board surface
- Lift up the bit after zeroing, before further moving
  
6. Set the XY origin
7. Cut --> Delete All --> Add the job files
8. Engraving --> Drilling --> Cutting
9. Left material into the leftover box, all tools and bits in the drawer
10. Clean the dust in the machine

## Outcome

{{<img "board0.jpg" "wide img_center">}}

At the first few attempts, the setting for the engraving job was not correct, and the machine failed to cut thin traces as well as pads for the USB C port. There are 4 potential reasons for this:

- The machine bed is not level
- Engraving too deep
- Too much contours
- A damaged bit was used

We changed all of them in the following jobs, cut the board in a different place on the machine bed(inner center is proved to be more level than the outer part) with a reduced engraving depth of 0.08mm, and a new engraving tool. This time the job was successfully finished. 


{{<img "engraving.jpg" "wide img_center" "Engraving" >}}
{{<img "cutting.jpg" "wide img_center" "Cutting">}}

{{<img "board1.jpg" "wide img_center" "Final cut">}}

## Soldering

{{<img solder.jpg "wide img_center">}}

For the soldering part I only did the pin sockets in order to test the OLED screen first (sadly it didn't work at this moment). 

Sockets are a little bit hard to solder cuz they have pins on both sides. Should add more extra contours around the pads to remove the copper near the pads.

## Tips from the future

{{<img "other_boards.jpg" "wide img_center shadow" "Other boards that I made during the input/output devices week">}}

After some trial and error with the PCB milling process, I have identified several tips for both PCB designing and milling that can lead to better results:

- Avoid holes and rivets if they are not necessary. Adding rivets is simple but it takes time, and sometimes you can destroy the board with a hard hit so you have to do it gently, which then leads to possible connectivity issues. 

- Compare to through-hole mount, surface mount header and pin sockets are also easier to route because their pins are spread into two directions, which gives more reason to avoid holes, but also makes them a little bit harder to solder.

- If holes are necessary, do it with 1.45mm. Bigger than 1.4mm because you need an offset to put the rivets in, smaller than 1.5mm to make sure that the rivet stick to the hole so when you flip the board around they don't fall off. Also, adding rivets to small holes means you are stretching and expanding the hole, which could break your board easily if your holes are aligned in a line and placed close to the edge.

- Solder small, flat component first, as larger components can take up more space and make it more difficult to maneuver around during the soldering process.

- Take orientation of the wires that attach to the Xiao board and other possible devices connected to the board in account while designing the PCB in order to prevent components and wires from overlapping with each other.

- Extra contours are very important. Especially around pads and holes. This will make soldering much easier and prevent short circuits.

- Sand the PCB material before milling. Sanding afterwards could result in damaged routes.