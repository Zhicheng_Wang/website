---
title: Molding and Casting
weight: 120
---

# Molding and Casting

## Intro

In this page, I will document the process of making a keycap using molding and casting.

---

## Designing

I started with a standard keycap model like this:

{{<img model1.jpg "img_center wide shadow" "front side">}}

{{<img model2.jpg "img_center wide shadow" "back side">}}

A mold was made by subtracting this model from a cube. Immediately I realized the walls of the model has angle which made it impossible to mill the mold.

{{<img mold1.jpg "wide img_center shadow">}}

After re-evaluating the shape of the model, I decided to flip the model around so that the cross-shaped peg is now facing upwards, thus providing the chance of making it a two-part mold.

For validating the design, I made a 3D printed mold to test it out.

{{<img test.jpg "img_center wide shadow">}}

Turns out the test was a complete failure, but I learned some valuable things:

- The shape for the upper part of the mold is too complicated. The central part broke inside the mold, probably because it's too thin.
- Print quality should be higher, in order to get a smoother finish, which will be much easier to release.

With that in mind, I changed the model design again. This time I changed from hollow to a solid infill design. Also all of the vertical faces are tilted with a small angle to avoid friction.

{{<img final_wax_model.jpg "img_center shadow" "Final wax model">}}

{{<img final_silicon_model.jpg "img_center shadow" "Final silicon model">}}

{{<img final_3d_printing_model.jpg "img_center shadow" "Final 3D printing model">}}


---

## Milling


Reference video:
{{<youtube bHglmCtTvhE>}}

Calculating feedrate:

{{<img feedrate_table.jpg "img_center wide shadow">}}

> **Feedrate = Number of flutes * Chip load * Spinal speed**

For this machine and tools used, nf = 2, ss = 14000, so

> **Feedrate = 2 * cl * 14000 = 28000 * cc**

### Foam

#### Flattening

Vcarve

- Measure the dimensions of the material
- Input Vcarve
- Create square, covers all the area
- Pocket toolpath, cut depth 10mm (or more)


| Tool         | Pass Depth | Stepover   | Feed Rate        | Plunge Rate     |
| ------------ | ---------- | ---------- | ---------------- | --------------- |
| 22mm End Mil | 2.0 mm     | 40%(8.8 m) | 2500 mm/min(max) | 500 mm/min(max) |

- Clear pocket --> Pocket allowance --> `-11mm`

{{<img pocketing_settings.jpg "img_center small shadow" "This is wrong">}}

- Save toolpath

Roland Modela MDX-40

- VPanel
- Make sure is on `G54` coordinate system (in `XYZ` and `Set origin point` section)
- Install tool
- Move material under the tool, lowering the tool
- Place the sensor
- Make sure sensor wire are connected and is under the tool
- `Set Z origin using sensor` --> Apply
- Move tool to start point
- `Set XY origin here` --> Apply
- Cutting speed --> 30%
- Cut --> Delete all --> Add `.nc` file --> Output
- Gradually add cutting speed

{{<video "flaten.mp4"  "wide img_center">}}


#### Roughing

Vcarve

- File --> Import 3D model
- Check orientation, size, central
- Z plane should be lower the lowest point of the pocket
- Hit OK
- Toolpaths --> Material Setup --> change if needed
- Toolpath --> roughing 

| Tool        | Pass Depth | Stepover | Feed Rate   | Plunge Rate |
| ----------- | ---------- | -------- | ----------- | ----------- |
| 6mm End Mil | 2.0 mm     | 40%      | 1960 mm/min | 490 mm/min  |

> **fr = 28000 * cc = 28000 * 0.07 = 1960mm/min**

- Machining limit boundary --> Model boundary
- Machining allowance --> 0.1mm
- Roughing strategy --> depends on your model

{{<video "foam_roughing.mp4" "wide img_center">}}

#### Finishing

| Tool             | Pass Depth | Stepover | Feed Rate   | Plunge Rate |
| ---------------- | ---------- | -------- | ----------- | ----------- |
| 3mm End/Ball Mil | 1.0 mm     | 0.5 mm   | 1260 mm/min | 315 mm/min  |

{{<video "foam_finishing.mp4" "wide img_center">}}



Reflections after the session:

- Foam milling should not only serve as the test cut for verifying the shape of the model, but also should be used as a reference in terms of time usage as the ratio between the feedrates used for cutting foam and wax is known.
- Roughing strategy for most "flat" design model could be `z level`, but for models that contains tiling vertical faces like mine, maybe `3D raster` is the better choice.



### Wax

#### Roughing

We use the chip load for steel here when milling wax to make sure that the machine can handle it.

> **fr = 28000 * cc = 28000 * 0.038 = 1960mm/min**

| Tool        | Pass Depth | Stepover | Feed Rate   | Plunge Rate |
| ----------- | ---------- | -------- | ----------- | ----------- |
| 6mm End Mil | 1.0 mm     | 40%      | 1064 mm/min | 226 mm/min  |

{{<video "wax_roughing.mp4" "wide img_center">}}

#### Finishing

For saving time I used the same 6mm tool for finishing because I want to get back home before sunrise😇. Lesson learned: reserve at least 4h for milling both the foam and the wax.

| Tool        | Pass Depth | Stepover | Feed Rate   | Plunge Rate |
| ----------- | ---------- | -------- | ----------- | ----------- |
| 6mm End Mil | 1.0 mm     | 0.5 mm   | 1064 mm/min | 226 mm/min  |


{{<video "wax_finishing.mp4" "wide img_center">}}

### Milling results

Overall it looks good but there's definitely stair-shaped edges on the vertical faces. Several reasons:
- 6mm tool rather than 3mm
- 0.5 stepover is too much
- `z level` rather than `3D raster`

Will look into those factors and see which is the deciding one next time milling.

{{<img "result1.jpg" "wide img_center shadow">}}

---

## Casting

[Darren](https://darrenbratten.gitlab.io/digital-fabrication_2022/wk11_moulding-and-casting.html) had a super detailed introduction for all the materials used in the casting. Shout out to him for making such a helpful documentation.

### Casting silicon

For the silicon mold I used the `Moldstar 15 slow`:
| Name                | Moldstar 15 Slow                                              |
| ------------------- | ------------------------------------------------------------- |
| Material type       | Platinum silicone rubber                                      |
| Product page        | [Link](https://www.smooth-on.com/products/mold-star-15-slow/) |
| Pot life            | 50 minutes                                                    |
| Mix ratio by weight | 1A:1B                                                         |
| Cure time           | 4 hrs at room temperature                                     |

{{<img "silicon_bottle.jpg" "half img_center shadow" "Sticky bottles">}}

{{<img "mixing_silicon.jpg" "half img_center shadow" "Looks tasty😇">}}

After mixing we did a long degassing session, probably 20-30 minutes which is significantly longer than what I expected.

{{<img "degassing.jpg" "half img_center shadow" "After degassing">}}

{{<img "pouring.jpg" "half img_center shadow" "Pouring">}}

{{<img "casting_slicon.jpg" "wide img_center shadow" "Curing">}}

### Casting plastics

For the actual model, I used the `Smooth cast 305`.
| Name                | Smooth cast 305                                             |
| ------------------- | ----------------------------------------------------------- |
| Material type       | Liquid plastic                                              |
| Product page        | [Link](https://www.smooth-on.com/products/smooth-cast-305/) |
| Pot life            | 7 minutes                                                   |
| Mix ratio by weight | 100A:90B                                                    |
| Mix ratio by volume | 100A:100B                                                   |
| Cure time           | 30 minutess                                                 |

{{<img "mixing_plastic.jpg" "half img_center shadow" "Mixing">}}

{{<img "casting_resin.jpg" "wide img_center shadow" "Curing">}}

## Final results

Casting process was successful. Removal of the 3D printed part was easier than I think, for I was worrying about the cross-shaped peg would be stuck inside and broke off if I try to pull it out.

{{<img "result2.jpg" "wide img_center shadow" "Removing from the silicon mold">}}

{{<img "result3.jpg" "wide img_center shadow" "Removing from the 3D printed mold">}}

Sanding could potentially remove the stair-shaped edges, but I think the current appearance is acceptable, as the strips now appear as if they are intentionally designed with that feature🤣.

{{<img "result4.jpg" "wide img_center shadow" "Final result (front side)">}}

{{<img "result5.jpg" "wide img_center shadow" "Final result (back side)">}}