---
title: Networking and Communications
weight: 130
---

# Networking and Communications

## Intro

 In this page I will document the process of trying out the wifi connection functions on the ESP32C3 board.

---

## Hardware

{{<img oled_board_irl.jpg "wide shadow img_center">}}

I continued with the last board I made for Input/Output device since it have all the function needed. The goal is to control the on board Neopixel and the connected OLED screen via internet, as well as get the button state on the board, thus making a REST API system.

## Programming

I used the example code provided by Kris as the starting point. The examples can be found in this Github repo:
- [XIAO ESP32C3 Examples](https://github.com/kr15h/XIAO-ESP32C3-Examples)

I prioritized adding the OLED display functions first, as this feature will greatly assist in the debugging process afterwards. To display the content that originally printed to serial on the screen, I write a short function that printed on both, and replaced the original `serial print` output codes with the new one.

```
void print_all(String content){
  display.println(content);
  Serial.println(content);
  display.display();
}
```

Then I changed the original digitalWrite code to the Neopixel controls.

```
if (query == "/turnLedOn") {
    response = "Turning LED on.";
    //digitalWrite(led, HIGH);
    pixels.setPixelColor(0, pixels.Color(255, 255, 255));
    pixels.show();

    display.setCursor(0,0);
    display.clearDisplay();
    display.println("Turning LED on");
    display.display();
} 
```
{{<expand "Full code">}}
```
#include <Adafruit_SSD1327.h>
#include <Adafruit_NeoPixel.h>
#include <WiFi.h>

// Settings for OLED
#define OLED_CLK SCK
#define OLED_MOSI MOSI
#define OLED_CS D2
#define OLED_DC D3
#define OLED_RESET D0
Adafruit_SSD1327 display(128, 128, OLED_MOSI, OLED_CLK, OLED_DC, OLED_RESET, OLED_CS);


// Settings for NEOPIXEL
#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif
#define PIN D1 // On Trinket or Gemma, suggest changing this to 1
#define NUMPIXELS 1 // Popular NeoPixel ring size
Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

// Settings for WIFI
const char* ssid     = "Fablab";
const char* password = "Fabricationlab1";   
WiFiServer server(80); // Webservers usually listen on port 80
int connectionCounter = 1;
int btn = D0;


void setup()
{
  pinMode(btn, INPUT_PULLUP);
  //pinMode(led, OUTPUT);

  Serial.begin(9600);


  //Initalizing the oled
  Serial.println("SSD1327 OLED test");
  
  if ( ! display.begin(0x3D) ) {
     Serial.println("Unable to initialize OLED");
     while (1) yield();
  }
  display.clearDisplay();
  display.display();
  display.setTextSize(1);
  display.setTextColor(SSD1327_WHITE);
  display.setCursor(0,0);
  display.setTextWrap(true);


  delay(10);

  // We start by connecting to a WiFi network
  Serial.println();
  print_all("Connecting to");
  print_all(ssid);


  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  display.println("WiFi connected");
  display.println("IP address: ");
  display.println(WiFi.localIP());
  display.display();

  server.begin(); // Start webserver

  pixels.begin(); // Start neopixel
  pixels.clear();
  
} 

void loop()
{
  // Do we have someone connecting?
  WiFiClient client = server.available();

  if (client) {
    Serial.println();
    Serial.println("Client connected.");
    //display.println("Client connected.");
    //display.display();

    if (client.connected()) {

      String incoming = "";
      while (client.available()) {
        char c = client.read();
        incoming += c;
      }

      //incoming = incoming.trim();
      //Serial.println(incoming);

      // Extract just the first line
      int charCounter = 0;
      String firstLine = "";
      while (incoming.charAt(charCounter) != '\n') {
        firstLine += incoming.charAt(charCounter);
        charCounter++;
      }

      Serial.println(firstLine);

      // Extract query
      charCounter = 0;
      bool firstSpace = false;
      String query = "";
      while (charCounter < firstLine.length()) {
        char c = firstLine.charAt(charCounter);
        
        if (c == ' ') {
          if (firstSpace) {
            break;
          } else {
            firstSpace = true;
          }
        }
        
        if (firstSpace == true && c != ' ') {
          query += c;
        }

        charCounter++;
      }

      Serial.println(query);

      String response = "";

      
      if (query == "/turnLedOn") {
        response = "Turning LED on.";
        //digitalWrite(led, HIGH);
        pixels.setPixelColor(0, pixels.Color(255, 255, 255));
        pixels.show();

        display.setCursor(0,0);
        display.clearDisplay();
        display.println("Turning LED on");
        display.display();
      } 
      
      else if (query == "/turnLedOff") {
        response = "Turning LED off";
        //digitalWrite(led, LOW);
        pixels.clear();
        pixels.show();

        display.setCursor(0,0);
        display.clearDisplay();
        display.println("Turning LED off");
        display.display();
      } 

      else if (query == "/getButtonState") {
        response = "Button state is ";
        int buttonState = digitalRead(btn);
        response += String(buttonState);

        display.setCursor(0,0);
        display.clearDisplay();
        display.print("Button state is: ");
        display.println(String(buttonState));
        display.display();
      } 
      
      else {
        response = "Unknown request: ";

        display.setCursor(0,0);
        display.clearDisplay();
        display.print("Unknown request: ");
        display.println(query);
        display.display();


        response += query;
      }

      client.println("HTTP/1.1 200 OK");
      client.println("Content-Type: text/html");
      client.println("Connection: close");
      client.println();
      client.println(response);

      /*
      client.println("<html>");

      int btnState = digitalRead(btn);
      client.print("Button down? ");
      client.print(btnState);
      client.println("<br>");
      client.print("This was attempt number ");
      client.print(connectionCounter);
      client.println("<br>");
      connectionCounter++;

      client.println("</html>");
      */

      // give the web browser time to receive the data
      delay(1);

      client.stop();
      Serial.println("Client disconnected");
    }
  }
}

void print_all(String content){
  display.println(content);
  Serial.println(content);
  display.display();
}
```
{{</expand>}}

## Result

Here's the result that I got.

{{<video result.mp4 "full shadow">}}

All in all, it's a simple try of the internet connectivity on the ESP32 board. Although I would have liked to explore this further, time constraints limited my progress. However, I plan to continue working on transmitting data gathered from the input devices through internet and visualize them during the upcoming [Interface and Application](/digital-fabrication/interface-and-application) week.