---
title: Computer-Controlled Cutting
weight: 30
---

# Computer-Controlled Cutting

## Intro

Last week, due to a bad ankle injury I have to stay at home for rest and didn't quite make it to the Fablab to implement my design with the Laser-cutting machine. However, as I have been mostly recovered now, I booked the machine for next Thursday and hopefully I can get my hand on the design and see if it works. 

---

## Difficulties
There are some potential problems that I would to adjust for the design, mainly about the cutting in parts have a angle to the vertical perpendicular, which means that the laser machine won't be able to cut such shapes.

{{< img 3.jpg "img_center shadow wide" >}}


## Potential Solutions

There are some solutions for this problem, however whether these solutions can be implemented and whether they can work needs further testing to prove.

### Flexible design
The first one is to make the part flexible so that they can be assembled even if the cut was vertical to the surface. Based on the test from [Darren Bratten](https://darrenbratten.gitlab.io/digital-fabrication_2022/wk5_computer-controlled-cutting.html), with special cuts that make these tiny "hinges", you can make a material flexible, and it surprises me that the amount of flex he tried on the plywood, which I think is more than sufficient for my design. 

{{< img 1.JPG "img_center shadow wide" >}}

The only thing that I concerned about is that since my parts are relatively smaller than his, so it remains a question whether my design would break easily if I implement this sort of flexible design.


### Use multiple pieces

{{< img 2.jpg "img_center shadow wide" >}}

Another way to implement this tilting cut is to use multiple pieces with slightly misalignment cuts to approach a tilting cut. The biggest limit for this method is that the thickness of the material is set, and the parts might be to thick if we use this method that combine them together.

### Final solution

For the initial solution, I try to simply widen the cuts in order to fit the whole angular cut design inside the vertical design. It surprises me that this solution actually worked. 

[example pic]()

---

## Implementation
TBD, will be finished after the testing and cutting session.

### Starting up and calibration

As the laser machine was precalibrated when I cut these pieces, no further work shall be done. I will add more about this part in the following sessions where I got to calibrate the machine myself.

### Testing


### Final results (v1)

Here's some pictures of the comparison between the rendered model and the actual result.


{{< img render3.jpg "img_center shadow wide" >}}

{{< img real3.JPG "img_center shadow wide" >}}

<br>

{{< img render4.jpg "img_center shadow wide" >}}

{{< img real4.JPG "img_center shadow wide" >}}

I think the result is pretty similar to the original design, although due to the lack of flexibility of the plywood, only eight part instead of nine was assembled to prevent extra high inner forces that would potentially make the parts fail. In the following sessions, I will continue working on this design, trying to bring the flexible design mentioned before to it and see if it works.