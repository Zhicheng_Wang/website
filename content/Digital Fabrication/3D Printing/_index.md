---
title: 3D Printing
weight: 50
---

# 3D Printing

## Intro

In this page, the process of designing and printing a model with 2 types of 3D prints would be introduced:
- **FDM** (Fused Deposition Modeling) printing
- **SLA** (Stereolithography) printing

This process contains the following sessions.
1. [Learning the settings and parameters for different printers.](/digital-fabrication/3d-printing/#printers-parameters-and-settings)
2. [Doing tests about supports, clearance, angle, overhang, and bridging.](/digital-fabrication/3d-printing/#testing)
3. [Design a model that could not be easily made with subtractive machining.](/digital-fabrication/3d-printing/#Designing-the-model)
4. [Print the model with two different printing methods.](/digital-fabrication/3d-printing/#printing-results)

[Fab Academy link for this week](http://academy.cba.mit.edu/classes/scanning_printing/index.html)

---

## Printers, parameters and settings

### Printers

Machines that we have in FabLab are from [Ultimaker](https://ultimaker.com/3d-printers) and [Formlabs](https://formlabs.com/3d-printers/catalog/).

For FDM printing, the available machines are:
- [Ultimaker S3](https://ultimaker.com/3d-printers/ultimaker-s3)
- [Ultimaker 2+ Connect](https://ultimaker.com/3d-printers/ultimaker-2-plus-connect)
- [Ultimaker 2 extended]()

For SLA printing, the available machines are:
- [Formlab Form 2](https://formlabs.com/3d-printers/form-2/)
- [Formlab Form 3](https://formlabs.com/3d-printers/form-3/)


### For FDA printing

#### Slicing

Slicing is the most crucial part of preparing a FDA printing job. After slicing we will get a preview that where you can visualize the whole job like this.

{{<video "UM2E_angle - UltiMaker Cura 2023-02-24 15-23-35_x264.mp4" "full img_center shadow">}}

There will probably be multiple slicing works while preparing for printing, since changing the parameters would require a re-slicing in order to take effect.

#### Infill density and type

For most printing jobs, 20% for density would be enough.

As for the different types, I tried slicing in preview with different options in order to see the differences. Here are the inner structures for different options.

{{<img infill-types.jpg "shadow img_center">}}

#### Build plate setting

Usually we use this setting to deal with adhesion. There is a good article that introduces the pros and cons for three most used adhesion assistants types: **brim**, **skirt** and **raft**.

- [3D Printing Raft vs Brim vs Skirt: The Differences](https://all3dp.com/2/3d-printing-raft-brim-and-skirt-all-you-need-to-know/)

As for my testing and model, there's no warping and adhesion issues, so I decided to go with the skirt type since it has the least material consumption and waste, as well as having the least additional time cost.

### For SLA printing

Similar to the FDA printing process, there are basic parameters such as quality and material for SLA printing, but other than that, there are some major differences.

#### Angle

For SLA printing, it is important to get the most optimized angle to save material and also get the best result. 

In Formlab PreForm, there is a option for auto-generating the angle, but I find the result of the auto-generated angle often have some issue, such as resulting in support generated on undesired areas. However, even if this auto-generated angle can not be the final angle for printing, it can still serve as the reference angle which you can begin with for fine tuning.

#### Support

PreForm also have the support auto-generating option. Like in angle selection, the generated supports would be the draft that we can edit and adjust based on. 

{{<img sla-edit-supports.jpg "shadow img_center">}}

There are basically two types of adjustments, first, removing redundant support points, and second, moving the support from the areas that you want to keep it clean to the unimportant parts. 

In this part we could have some issues, in which after the original support was removed, the structure could not be stable enough to print even with the additional supports in another place. In this case we have to roll back and make do with the original one, or changing the printing angle to see if that helps.

---

## Testing

Multiple tests for the FDA printers were done either be me or by other students. I did the angle test (without supports) especially, and the results are better than I expected.

{{<img angle-1.jpg "wide shadow img_center">}}

From the results we can see that in this scale, the FDA printer can handle printing job up to almost 90 degrees, with limited loss in detail and deformation.

{{<img angle-2.jpg "wide shadow img_center">}}

Other testing about the bridging and overhang are also implemented. This test done by Anqi Wang showed that in the maximum distance which is 20mm, some deformation started to occur, especially the lower part where the first lines of material are placed just in air with no support.

{{<img bridging.jpg "wide shadow img_center">}}

Unfortunately, I did not do the testing session on the SLA printer though since it takes much longer time than the FDA ones, and probably they have completely different characteristics due to the physical properties of the two printing methods. Some of the testing model would have to be revised in order to be better fit for the SLA printer.

---

## Designing the model

{{<img blender-geo-nodes.jpg " shadow img_center">}}

For the actual printing model, I used blender's geometry node, creating a abstract geometry shape that consists multiple rotating half spheres. I adjusted the angles so that there are overlaid surface from every angle, thus it would be very hard to manufacture with traditional machining.

{{<img render.jpg "wide shadow img_center">}}

Here's a render picture of the ideal result of the model (with extra unnecessary lighting to make it seems artistic lol).

---

## Printing results

Printing results are somewhat mixed on both the FDA and the SLA printers.

### FDA results

On the first try, I tested the Ultimaker 2+ Connect printer with the transparent purple filament. 

{{<video "FDA-1.mp4" "full img_center shadow">}}

From the video you could probably guess the result what not quite good. There are loads of adhesion between the layers which I spend quite a lot of time removing.

{{<img FDA-1.jpg "wide shadow img_center">}}

After cleaning, the model looks somewhat better, but still not the ideal result that I wanted.

{{<img FDA-1.5.jpg "wide shadow img_center">}}

So I tired for the second time on the Ultimaker S3 with solid yellow filament. 

{{<video "FDA-2.mp4" "full img_center shadow">}}

To prevent the adhesion issue from happening again, I scaled the model up for about 10~15mm, and increased speed in the settings. The results are much better with this time around.

{{<img FDA-2.jpg "wide shadow img_center">}}

So to conclude, there are some possibilities that cause the first attempt to fail.
- Printer (2+ Connected vs S3)
- Filament (Transparent purple vs Solid yellow)
- Speed
- Scale (distance between different layers)

### SLA results

On the first try for the SLA results, I noticed that all the layer are smashed together instead of placed separately as they should be. The quality and the surface finish is much better than the FDA printed one though. 

Although being significantly smaller than both of the FDA printed ones, this still takes about 3 hours to print, 4 if you count the washing and curing process in.

{{<img SLA-1.jpg "wide shadow img_center">}}

So on the next try, I also scaled the model up for about 10mm (from 20mm to about 30mm). This caused much more complicated support management issue, where I struggled to find a proper angle that do not have support on the front side of the model.

{{<img SLA-2-1.jpg "wide shadow img_center">}}

Looks good when I first removed it from the build plate despite still having the same inseparable layer problem.

{{<img SLA-2-2.jpg "wide shadow img_center">}}

But after washing and curing, I think we can see some of the layer is not that obvious compare to the first one. I'm not sure if the washing or the curing process have something to do with it.

So the possible cause for this issue are:
- Resin type (Tough 1500 vs Black V4)
- Printer (Form 2 vs Form 3)
- Placement (I think I forget to place the model in the corner in the second attempt)
- Washing and curing process
- Scale (the second SLA attempt is still smaller than the first FDA print)

Overall, I think the SLA printers might have significant finer details and smoother surfaces compared to the FDA ones, but it had it's own limitations and shortcomings such as long printing time, much complicated supports (which are also harder to remove), and struggled with this separating issue.

{{<img SLA-2-3.jpg "wide shadow img_center">}}