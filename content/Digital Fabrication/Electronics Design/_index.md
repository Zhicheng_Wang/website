---
title: Electronics Design
weight: 60
---

# Electronics Design


## Intro

In this page, the process of designing a PCB board would be documented. This would be separated into three parts.

- [Requirements](#requirements)
- [Schematic Design](#schematic-design)
- [PCB Design](#pcb-design)

---

## Requirements

The first thing to do for designing a board is to do the requirements analysis. As the goal is to design a bluetooth extension board for a keyboard, the following requirements would be fulfilled:

| Requirements                            | Possible Solutions |
|-----------------------------------------|--------------------|
| Bluetooth connectivity                  | XIAO nRF52840      |
| Receive inputs & power for the keyboard | USB Type-C port    |
| Inputs for the board                    | Switch             |
| LED                                     | NeoPixel THT       |
| Display                                 | SSD1306 OLED       |
| Serial / Battery                        | 2*2 PinHeader      |
| QMK / ZMK firmware support              | ?                  |

After the requirements and their possible solutions are listed, we can go over some part for more information.


### XIAO nRF52840

There are two ICs that supports wireless communication in the library:

- XIAO ESP32C3 which supports WiFi and Bluetooth 5 (LE)
- XIAO nRF52840 which supports Bluetooth 5.0/BLE and NFC

Since we only concerned about bluetooth connectivity, the nRF52840 was chosen for this board because it's lower energy consumption compared to the ESP32, which might beneficial since we are doing a battery powered project.

{{<youtube vrcPGeYinVQ>}}

### USB Type-C port

To enable communication between the board and the keyboard, as well as providing the power for the keyboard to work, a Type-C port shall be added to the board.

This port might be deleted in the final version since I found that the keyboard that I wish to connect to the board have a daughter board, some this board could potentially replace it, thus no dedicated Type-C port would be needed.

{{<img "daughter-board.png" "img_center wide shadow">}}


### Display

There are multiple needs for a having a display:

- Battery info
- Keyboard status
- Bluetooth connection info

Meanwhile, three displays are available according to the [Fab Lab Inventory](http://inventory.fabcloud.io/):

- **20X4** LCD Display LCD Screen I2C Serial
- 0.96 Inch OLED Display Module, I2C Serial **128 x 64** with **SSD1306**
- 1.3inch IPS 65K Full Color 3.3V **240x240** LCD Screen with SPI Interface **ST7789** IC Driver


The OLED display with SSD1306 driver was chosen because of it's higher resolution, better readability, and the most reasonable size.

---

## Schematic Design

This is the complete schematic design of the first version for the board. Modular design was implemented to make a cleaner layout. 

![schematic](schematic.jpg)

### IC

The pin layout for the XIAO nRF52840 board is showed below.

{{<img "nrf52-pins.png" "img_center wide shadow">}}

As `D4` to `D10` all have dedicated functions, `D0` to `D3` was utilized for this board design.

{{<img "ic.jpg" "img_center wide shadow">}}
<br>

### USB Type-C



To design the USB Type-C properly, we need to first know the functions of each pin in the port.

![pins](type-c-pins.jpg)

This article provided an in-depth introduction for each of the pins and how to use them.
[Guide to USB-C Pinout and Features](https://www.allaboutcircuits.com/technical-articles/introduction-to-usb-type-c-which-pins-power-delivery-data-transfer/)

{{<img "usb-refer.jpg" "img_center wide shadow" "Schematic of the USB-C port on the XIAO board">}}

For the USB Type-C port, I just copied the design on the XIAO board, since both of them are using only the USB 2.0 protocol. In this case, only the `d+` and `d-` pin would serve as the data transmitting port that need to connect to the IC. For the rest of the pins, I just followed the reference design with proper power connection.

{{<img "usb.jpg" "img_center wide shadow" "Schematic of my own design">}}

### PinHeaders and Switch

There's nothing much to say about the design for those components. 

{{<img "pinheaders-and-switches.jpg" "img_center wide shadow">}}

The `TX` and `RX` pins are connected to the corresponding pins on the IC board. The 2*2 PinHeaders would probably also serves as the power input, especially for the 5V input that was coming from the battery.

Noted that no pull-up resisters needed for the button since they are embedded inside the IC.

### NeoPixel LED

A NeoPixel LED was added to the board as the indicator. There's no resistor since the NeoPixel is using 5V power.

{{<img "neopixel.jpg" "img_center half shadow">}}

The `DIN` and `DOUT` pins are used for controlling the LED, the `DOUT` would be especially useful when controlling a series of LEDs, but since here we only use one of them, it was just left empty.


### OLED screen

I did not find the nor the schematic or the footprint for the OLED display in the fab library, thus I used [another library from github](https://github.com/pforrmi/KiCad-SSD1306-128x64).

{{<img "OLED.jpg" "img_center half shadow">}}

The connection are also fairly simple. The `SCL` and `SDA` pins were connected with the corresponded pin on the IC board, similar to the pin headers.

---

## PCB design

Multiple iterations for PCB design have been made in this process. 

The biggest change within this process is that I gave up on the copper pour idea since I found that it was actually not necessary for this board design, and adding it was more trouble than it's worth. 

So, in this way, the PCB design can be separated into two different versions.


### Version 1: with copper pour

{{<img "PCB1.jpg" "img_center shadow">}}

Despite being messy, there are multiple problems in this version. 

1. Pins on the 2*2 headers are connected with the wrong pins on the IC board. 
2. Copper fill was added in order to get the GND to different parts of the board, but in some areas it was too close or have overlays with the wires, which will cause unwanted connections.
3. The USB Type-C port was not placed on the edge of the PCB as it should, which might cause the USB plugs unable to be inserted.

### Version 2: without copper pour

{{<img "PCB2.jpg" "img_center shadow">}}

After getting rid of the copper pour and adjusting the placement as well as the orientation of multiple components, the second version of the PCB was made. It was both cleaner and smaller than the original one, which is a good thing since we might have limited space in the real working scenarios.

{{<img "PCBR.jpg" "img_center shadow">}}

3D render looks neat. Pretty satisfied with this board design.

---

## References

### Parts
[Fab Lab Inventory](http://inventory.fabcloud.io/)

### KiCAD

[Bus, labels, and global labels](https://www.baldengineer.com/kicad-bus-labels-and-global-labels.html)

[KiCAD library for Fablab](https://gitlab.fabcloud.org/pub/libraries/electronics/kicad)

### OLED

{{<youtube c64WG4iJuEo>}}


[OLED instruction](https://randomnerdtutorials.com/guide-for-oled-display-with-arduino/)

### NeoPixel

[Datasheet](https://cdn-shop.adafruit.com/product-files/1138/SK6812+LED+datasheet+.pdf)

### XIAO AND BLE

[Seeed Studio XIAO nRF52840 index page](https://wiki.seeedstudio.com/XIAO_BLE/)

[Seeed Studio XIAO nRF52840 Schematic](https://files.seeedstudio.com/wiki/XIAO-BLE/Seeed-Studio-XIAO-nRF52840-Sense-v1.1.pdf)

[Nordic Semiconductor documentation for nRF52840](https://infocenter.nordicsemi.com/index.jsp?topic=%2Fcom.nordic.infocenter.sdk5.v14.2.0%2Fuart_example.html&cp=4_0_0_4_6_44)

[State of BlueMicro 2022](http://bluemicro.jpconstantineau.com/blog/)

[Getting QMK on nrf52](https://github.com/joric/nrfmicro/wiki/QMK)

[sekigon-gonnoc's lib for nrf52 QMK drivers](https://github.com/sekigon-gonnoc/qmk_firmware/tree/nrf52/drivers)

[nRFMicro (nRF52840-based, hardware USB support) vs Jian keyboard](https://www.reddit.com/r/MechanicalKeyboards/comments/bby5y4/nrfmicro_nrf52840based_hardware_usb_support_vs/)