//Original code
//For the #WCCChallenge, theme: "squishy"
let r;
let layers = [];
let bg = 255;
let tbg = 0;
let fcol;
let lean = 0;
let lvel = 0;
let stretch = 0;
let svel = -5;

let pax = 0;
let pay = 0;
let paz = 0;
let ax = 0;
let ay = 0;
let az = 0;

let pacx = 0;
let pacy = 0;
let pacz = 0;
let acx = 0;
let acy = 0;
let acz = 0;

// Get Gyro data from the Xiao board
function getData(){
    fetch('http://193.167.5.162/json')
    .then(response => {
        if (!response.ok) {
        throw new Error('HTTP error, status = ' + response.status);
        }
        return response.text();
    })
    .then(textData => {
        // Transform the text into JSON
        let jsonData = JSON.parse(textData);

        // Access the JSON data
        ax = jsonData.angleX;
        ay = jsonData.angleY;
        az = jsonData.angleZ;


        acx = jsonData.accX;
        acx = jsonData.accY;
        acx = jsonData.accZ;
        // Print the data to the console
        //console.log('ax:', ax);
        //console.log('ay:', ay);
        //console.log('az:', az);

        //return ax,ay,az;

    })
    .catch(error => {
        console.error('Error:', error);
    });
}




function setup() {
    let cnv = createCanvas(600, 600);
    cnv.parent('canvas-container');
    r = height / 3;
    for (let y = height / 3; y > -height / 3; y -= height /36) {
        let h = map(y, height / 3, -height / 3, 3, 4);
        let verts = [];
        for (let a = 0; a <= TAU; a += TAU / 30) {
            verts.push(new Vert(a, r, createVector(0, 0),y));
        }
        layers.push(new Layer(verts, h));
    }
    fcol = color(random(255), random(255), random(255), 100);

    //pax,pay,paz = ax,ay,az;
    getData();
}

function draw() {
    pax,pay,paz,pacx,pacy,pacz = ax,ay,az,acx,acy,acz;
    getData();
    console.log(ax,ay,az,acx,acy,acz);
    lean += lvel;
    lvel += (ax - pax) / (height * 50);
    lvel -= lean / 20;
    lvel *= 0.85;
    stretch += svel;
    svel += (ay - pay -1) / (height * 50);
    svel -= stretch / 20;
    svel *= 0.9;
    bg = lerp(bg, tbg, 0.1);
    background(bg);
    fill(fcol);
    stroke(255, 100);
    strokeWeight(height / 220);
    translate(width / 2, height / 2);
    for (let l of layers) {
        l.jiggle();
    }
}

/*
function mousePressed() {
	tbg = 255 - tbg;
	if (tbg == 0) fcol = color(random(255), random(255), random(255), 100);
}
*/

class Layer {
	constructor(verts, h) {
		this.verts = verts;
		this.h = h;
	}
	jiggle() {
		rotate(lean);
		translate(0, stretch);
		beginShape();
      
		for (let v of this.verts) {
			v.r += v.rv;
			v.rv -= (v.r - r) / 10;
          
			// if (movedX!=0 || movedY!=0 ) {
			// let d = dist(v.pos.x, v.pos.y, mouseX - width / 2, mouseY - height / 2) / 2;
			// v.rv -= r / max((height / 50), d);
			// }
			v.rv *= 0.9;
			v.pos.x = v.r * cos(v.a);
			v.pos.y = v.basey + (v.r / this.h) * sin(v.a);
			curveVertex(v.pos.x, v.pos.y);
		}
        
		endShape(CLOSE);
	}
}

class Vert {
	constructor(a, r, pos, basey) {
		this.a = a;
		this.r = r;
		this.rv = 0;
		this.pos = pos;
		this.basey = basey;
	}
}
