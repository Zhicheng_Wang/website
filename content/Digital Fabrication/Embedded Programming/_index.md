---
title: Embedded programming
weight: 40
---

# Embedded Programming


## Intro

In this page, I will introduce how I made and tested the Xiao board. This will be divided into three stages.

- [Preparation (hardware and software)](#preparation)
- [Testing (NeoPixel LED)](#testing-neo-pixel)
- [Final (Adding serial connectivity)](#final-adding-serial-connectivity)

---

## Preparation

### Hardware: Soldering

Soldering this board is harder than I thought. Struggled to manage to keep it nice and clean while making all the solder joints in same size.

{{< img soldered-board.jpg "img_center shadow half">}}

Fortunately, the final outcome seems nice and clean. Satisfied.

### Software: Arduino IDE

First thing to do this to install the board by add the [configuration file url]().

{{< img add-board-url.jpg "img_center shadow ">}}

Then install the board in the IDE. Search for "rp2040".

{{< img install-rp2040-in-ide.jpg "img_center shadow ">}}

Select the right board and port.

{{< img select-board-and-port.jpg "img_center shadow ">}}

Install Neo-pixel library.

{{< img install-neopixel-lib.jpg "img_center shadow ">}}

---

## Testing: Neo-pixel

I made minor changes to the example program for the Neo-pixel library, making the color change gradually rather than directly. The color still have big shift when the value hit 255, but that would be easily fixed. 

{{< img gradient-neopixel-led.jpg "img_center shadow ">}}

---

## Final: Adding serial connectivity

With the final program, I decided to add serial button detection and led all in one program.

{{< video showcase.mp4 "img_center shadow wide">}}

The program imitates the basic operation of a web server. The led serve as a identification, button as the power button to switch it on/off, and use serial to communicate with the client which is the serial port on the computer.

### State 1: Off

When the server is off, the identification light would go off, and if you try to send something to the board via serial, the server will return a message: `"Standing by, please press the button."`

### State 2: On

When the server is on, the light would be turned on and change the color gradually. In this state if the board received a message, it will send the message back with a `"Received:"` prefix. 

Press the button once would change the state of the server. When the state is changing, the board will notify the client via serial, sending `"System shut down"` or `"System back online"`.

### Source code

```
#include <Adafruit_NeoPixel.h>
#define BUTTON D10
#define NUMPIXELS 1

int previous_state = 1; 
bool flag = true;

int Power = 11;
int PIN  = 12;

int r = 0;
int g = 127;
int b = 255;
 
Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);
 
void setup() {
  pinMode(BUTTON, INPUT_PULLUP);
  pixels.begin();
  pinMode(Power,OUTPUT);
  digitalWrite(Power, HIGH);
  Serial.begin(19200);
}
 
void loop() { 
  // Serial.print("rea: ");
  // Serial.println(digitalRead(BUTTON));
  // Serial.print("flag: ");
  // Serial.println(flag);
  // Serial.print("prevs: ");
  // Serial.println(previous_state);
  

  if(previous_state != digitalRead(BUTTON) && previous_state == 1){
    flag = !flag;
    if(flag==1){
      Serial.println("System back online.");
    }
    else{
      Serial.println("System shut down.");
    }
  }
  previous_state = digitalRead(BUTTON);

  if(flag == true){

    if(Serial.available()){
      String message = Serial.readString();  //read until timeout
      message.trim();
      Serial.print("Received: ");
      Serial.println(message);
    }

    pixels.clear();
    r = (r+3)%255;
    g = (g+2)%255;
    b = (b+1)%255;
    pixels.setPixelColor(0, pixels.Color(r, g, b));
    delay(10);
    pixels.show();

  }


  else{
    if(Serial.available()){
      Serial.readString();
      Serial.println("Standing by, please press the button.");
    }
    pixels.clear();
    //pixels.setPixelColor(0, pixels.Color(0, 0, 0));
    pixels.show();
  }
  
}
```