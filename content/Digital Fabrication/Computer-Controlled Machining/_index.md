---
title: Computer-Controlled Machining
weight: 70
---

# Computer-Controlled Machining


## Intro

In this page, I will document the process of using the CNC milling machine to make a wood laptop stand.

This process would be separated into three main sections:

- [**CNC Machining Workflow**](/digital-fabrication/computer-controlled-machining/#workflow)
- [**Designing**](/digital-fabrication/computer-controlled-machining/#design)
- [**Results**](/digital-fabrication/computer-controlled-machining/#results)

---

## Safety

{{<hint danger>}}
CNC machine can be very dangerous and harmful: 
- Tools are dangerous while spinning.
- Tools break really easily if the settings were wrong, damaging people and machine. 
- Potential of catching fire if the leftover materials collected by the dust collectors is in high temperature.

To prevent those unwanted situations from happening, several rules must be followed in the machining process:

- Wear protective equipments, avoid any loose parts from clothes (and hair also).
- Double check all the settings and setup procedures before cutting.
- **Always have someone watching over the machine while cutting.**
- If anything looks/sounds/smells wrong, immediately stop the machine.
{{</hint>}}

## References

{{<hint info>}}
**Documentation from Aalto Wiki:**<br>
[**CNC Milling with Recontech 1312**](https://wiki.aalto.fi/display/AF/CNC+Milling+with+Recontech+1312)
{{</hint>}}

I primarily refereed to this documentation from Aalto WIKI written by Darren Brattan during my use of the Recontech 1312 machine, and also formed the structure of my own documentation based on that. [Darren's personal documentation](https://darrenbratten.gitlab.io/digital-fabrication_2022/wk9_computer-controlled-machining.html) is also very much helpful. 

There is also the tutorial video that we can refer to if detailed demonstrations are needed.


{{<hint info>}}
**Video tutorial**<br>
{{<youtube mRKgx9oFAFw>}}
{{</hint>}}

A huge thanks to Darren, Kris, Fiia, and everyone who contributed to the tutorials which made the process of CNC easier and safer.

---

## Workflow

According to the wiki page, there are 7 steps when using the cnc machine, which are:
- [Computer-Controlled Machining](#computer-controlled-machining)
  - [Intro](#intro)
  - [Workflow \& *Safety*](#workflow--safety)
    - [Calculate the feed-rate](#calculate-the-feed-rate)
    - [General Operation](#general-operation)
    - [Using design software](#using-design-software)
    - [Loading the tools](#loading-the-tools)
    - [Preparing for the bed](#preparing-for-the-bed)
    - [Fixturing](#fixturing)
    - [Calibration](#calibration)
    - [Launching](#launching)
    - [Finishing up](#finishing-up)
    - [Material](#material)
  - [Design](#design)
  - [Results](#results)

I will go through each of them in the following sections but in slightly different order.

### Calculate the feed-rate

One of the most important parameter for CNC cutting is the feed rate. The feed rate is calculated based on the following formula:

> **Feed rate (mm/sec) = Number of Flutes * Tooth feed (mm) * RPM (mm/s)**

For this cutting session, we are using 

- 6mm downcut tool with two flutes
- 15mm-thick plywood material (soft wood)
- 16000 RPM (max can reach 20000 but lowered for safety)

According to this sheet the tooth feed should be 0.06mm.

![Alt text](tooth-feed.jpg)

Therefore, the feed rate should be:

> Number of Flutes * Tooth feed (mm) * RPM (mm/s) = 2 * 0.060 * 16000 = **1920(mm/min)**

The plunge rate should be one quarter of the feed rate:

> 1920 / 4 = **480(mm/min)**

### General Operation

{{<img "machine-controller.JPG" "full shadow img_center" "Demonstration of the operating console. Picture from Aalto WIKI documentation page.">}}

To start the CNC machine:

1. Rotate the main switch to the "ON" position
2. Press "Power ON" button & wait for 15 seconds for it to power on
3. Opening the Mach 3 software
4. Hit "RESET" (1), "ENABLE SOFT LIMITS" (2), and "REF ALL HOME (3)"


{{<img "mach3.PNG" "full shadow img_center" "Demonstration of the Mach 3 software UI. Picture from Aalto WIKI documentation page.">}}

When the machine has stopped, you may now also move the machine with the arrow keys. 

> Move in x axis: up/down arrow key 
> 
>Move in y axis : left/right arrow key
>
>Move in z axis: page up/down key

### Using design software

For 

### Loading the tools

The tool that we are going to mainly use is the 6mm two-flute flat-end milling tool. 

- Drilling / Milling

Unlike the drilling tool which can only go vertically, the milling tool is able to engrave materials and move on a surface horizontally, however it needs a little bit more space to do the drilling, thus the diameter of the hole should be slightly bigger than the tool. 

- Downcut / Upcut

The tool that I was using for the project is a downcut tool. 

The main difference between those two is that downcut brings the leftover materials downward, and upcut brings them upwards. Downcut tools would give a nicer top surface finish, while the upcut tools will give a better bottom surface. Also, the upcut tools are also better at removing leftover materials since they bring them to the top surface where the dust collector is.

Thus, the most ideal way is to use the downcut tool for the first few layers, and then use upcut for the rest. Unfortunately, there was only one 6mm downcut tool left when I was cutting, so I have to stick with that. Luckily the tool is relatively new, so both the top and the bottom surface is quite clean.

- Loading the tools

Loading the tools correctly is crucial for a successful cutting process. A not secured installation might result in danger, damaging the tool as well as the machine. General process are as follows:

1. Place the cushion
2. Unscrew the nut
3. Snap the collector (according to the tool diameter)
4. Lightly screw it back
5. Put the tool inside the collect, 1.5-2cm
6. (Tighten the bit with hands to make sure the tool won't fall)
7. Tighten the bit as much as possible with the wrench

### Preparing for the bed

It seems that the last person using the CNC machine before me did not reset the vacuum bed setup, so all I have to do is to place the MDF sacrificial layer on the bed and open the valves. Although that does saved some time for me, it's definitely not the ideal way and we should reset all things back to original state after we finished. Anyway, the vacuum bed preparation process includes the following steps in normal conditions:

1. Place rubber strips under the cutting area (sealed area should be smaller than the material)
2. Unplug the vacuum holes in those areas
3. Place the sacrificial layer, remove dusts with the air compressor
4. Open valve(s) 
5. Activate the vacuum pump and check the pressure gauge 

### Fixturing

{{<hint warning>}}
Over-fixturing is always better than under-fixturing.
{{</hint>}}

There are mainly four ways to add more fixtures:

1. Screws/nails: with a thick sacrificial, you can attach screws to the edge, for example
2. Clamps: In the small locker there are clamps you can use - they attach to the rails (see image below)
3. Glue/tape
4. Weights
5. Encapsulation (~using surrounding pieces)

Learning from the prior instances of both successful (and more likely unsuccessful) cutting experiences of others, it was observed that the vacuum bed alone was insufficient for securing the material in place. This inadequacy was demonstrated by a tendency for the material to shift during the cutting process, resulted in deviated cutting paths and damaged tools. Thus, I used 4 clamps on each corner to reinforce the material.

### Calibration

Calibration for Z axis is the most important.
- Move the tool to some place where within reach
- Lower the tool down a bit (but not too low)
- Place the calibrator underneath the tool, should be on top of material and sacrificial layer
- Open the valve(s) for the vacuum bed (for the needed area)
- Activate the vacuum pump
- Make sure now that the material is stiff
- Click on the `Terän mittaus` button, wait for the tool to touch the calibrator
- Deactivate the vacuum pump
- Put the calibrator back to the slot

After that, move the XY axis to define the zeros. Make sure that the tool path would not touch the clamps.

### Launching

- Put the dust shoe back on
- Open the vacuum pump, check the fixture
- Go outside, close the door
- Load G-code, check the tool path
- Press the green button

### Finishing up

- When the light is green again, wait for a bit for spindle to stop spinning before reentering
- Deactivate the vacuum from the machine controller
- Move tool into a comfortable position with the keyboard
- Vacuum up any chips/dust when removing your workpiece
- Place the cushion under the tool
- Use the spanner and key to loosen the tool, and 'push' the collet from the nut
- Place all tools and bits back where you got them
- Reset the rubbers
- Use the Power Off on the machine controller & wait for a few seconds until it's small display is blank
- Rotate the main switch to an OFF position


### Material

{{<hint warning>}}
Make sure to put the **gloves** on when handling wood material.
{{</hint>}}

We have a 1200\*1200\*15mm piece of plywood sheet that we can use for this project.


## Design

For the actual design, I want to make some thing that shares the same aesthetic with the traditional chinese furniture, which many of them utilized wood joints design. But many of those structures were to complicated and needs 3D milling to make. So instead I chose a more simple pattern design and tried to implement in my product, which is a laptop stand. 


![Alt text](refer.jpg)

![Alt text](refer-pattern.jpg)

At first I put too much time into figuring out how we can (or if we can) generate those pattern in Fusion 360, but it was too hard especially on edges where the geometry can be pretty tricky. But finally I get the pattern done and modeled it in fusion.

{{<img final_design.jpg  "img_center">}}

In order to prevent the walls from wobbling, I designed a type of joint that can be easily constructed with CNC.

{{<img test_piece.jpg  "img_center">}}

I cut some test demos to verify the design, it was really stiff and I couldn't disassemble it after assembled.

{{<img test_piece_together.jpg  "img_center">}}

## Results

{{<img cut_result.jpg  "img_center">}}

The cut result was pretty good, all the path are cut through and surface are pretty clean. However,

- The other side of the board have a better surface, so it would be better to cut on the other side
- Tabs inside the pattern are difficult to remove

{{<img sanding.jpg  "img_center">}}

Sanding with 100 and 200 paper.

{{<img varnish.jpg  "img_center">}}

Adding oil varnish. I added two layers and sanded between those two varnishing sessions. Hard to get rid of the smell though.

After washing with water several times and placing in a ventilated environment for two weeks, the smell is finally gone and I think it would be safe to use in my apartment.

{{<img result.jpg  "img_center">}}

In the final use scenario, I removed one of the walls because I found that the groove for the wall is also perfect for putting my other laptop in. So now it's a two-slot laptop stand. That's *the unexpected virtue of ignorance* I guess. 

Although the patterns on the walls were completely hidden behind is a unfortunate aspect of the design, overall it still fulfilled my intensions (and even exceeding a little bit) and I'm pretty satisfied with the results.